// msc-lzw.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <string.h>
#include "main.h"
#include "dictionary.h"

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>	



// alternate FNV-1a
// Swap the order of the * and ^: 
// instead of h = (h * 16777619) ^ p[i] 
// Do this    h = (h ^ p[i]) * 16777619
//
// Currently used only for the case of the string key, I use it to calculate
// hash of the byte string.  I have also tweaked it to scale down to 32-bit
// using a variant of the von neuman middle square pseudorandom number generator
// in order to get a 32 bit number.  Strike that. on windows long is 32-bit.
//
unsigned int FNV_hash(void* dataToHash, unsigned long int length) {

	unsigned char* p = (unsigned char *)dataToHash;
	unsigned long int h = 2166136261UL;
	unsigned long int i;
	unsigned int rc;

	for (i = 0; i < length; i++)
		h = (h * 16777619) ^ p[i];

	rc = h;

	return rc;
}


// Another hash function
//The hash code for a String object is computed as
//
//s[0] * 31 ^ (n - 1) + s[1] * 31 ^ (n - 2) + ... + s[n - 1]
//using int arithmetic, 
// where s[i] is the ith character of the string, 
// n is the length of the string, and
// ^ indicates exponentiation. (The hash value of the empty string is zero.)
// 
// another guy says to use 92821 instead of 31 as the chosen prime.
// https://stackoverflow.com/questions/1835976/what-is-a-sensible-prime-for-hashcode-calculation/2816747#2816747



// Its used to find a slot based upon an integer.  This was done as opposed to managing multiple keys 
// mapping to the same slot and then storing them in a linked list.  That approach requires knowing
// which element in the list maps to what.
unsigned int find_dict_slot(unsigned int key, unsigned int try_num, unsigned int max) {
	return (key + try_num) % max;  // the hash fun returns a number bounded by the number of slots.
}



int hash_insert_1(struct key_value_pair_int_key_str_val *pKeyValuePair, struct hash_table_int_key_str_value *hash_table) {

	unsigned int try_num, hash;
	unsigned int max_number_of_retries = hash_table->max;


	if (hash_table->number_of_elements >= hash_table->max) {
		return 0; // FULL
	}

	for (try_num = 0; try_num < max_number_of_retries; try_num++) {

		hash = find_dict_slot(pKeyValuePair->key, try_num, hash_table->max);
		if (NULL == hash_table->elements[hash]) { // an unallocated slot
			hash_table->elements[hash] = pKeyValuePair;
			hash_table->number_of_elements++;
			return RC_OK;
		} else {
			printf("collision %d. will retry %d.\n", hash, try_num);
		}
	}
	return RC_ERROR;
}


int hash_insert_2(struct key_value_pair_str_key_int_val *key_value_pair, struct hash_table_str_key_int_value *hash_table) {

	int try_num, hash;
	int max_number_of_retries = hash_table->max;


	if (hash_table->number_of_elements >= hash_table->max) {
		return 0; // FULL
	}

	for (try_num = 0; try_num < max_number_of_retries; try_num++) {

		hash = find_dict_slot(key_value_pair->keyHash, try_num, hash_table->max);

		if (NULL == hash_table->elements[hash]) { // an unallocated slot
			hash_table->elements[hash] = key_value_pair;
			hash_table->number_of_elements++;
			return RC_OK;
		}
	}
	return RC_ERROR;
}




// returns the corresponding key value pair struct
// If a value is not found, it returns null
//
// 32-bit version - actually windows is not like linux. long int and int are the same size on windows.  see the FNV hash for sizeof() results
struct key_value_pair_int_key_str_val *hash_retrieve_1(unsigned int key, struct hash_table_int_key_str_value *hash_table) {

	unsigned int try_num, hash;
	unsigned int max_number_of_retries = hash_table->max;

	for (try_num = 0; try_num < max_number_of_retries; try_num++) {

		hash = find_dict_slot(key, try_num, hash_table->max);

		if (hash_table->elements[hash] == 0) {
			return NULL; // Nothing found
		}

		if (hash_table->elements[hash]->key == key) {
			return hash_table->elements[hash];
		}
	}
	return NULL;
}


// returns the corresponding key value pair struct
// If a value is not found, it returns null
//
// 32-bit version
struct key_value_pair_str_key_int_val *hash_retrieve_2(int keyHash, struct hash_table_str_key_int_value *hash_table) {

	unsigned int try_num, hash;
	unsigned int max_number_of_retries = hash_table->max;

	for (try_num = 0; try_num < max_number_of_retries; try_num++) {

		hash = find_dict_slot(keyHash, try_num, hash_table->max);


		if (hash_table->elements[hash] == 0) {
			return NULL; // Nothing found
		}

		if (hash_table->elements[hash]->keyHash == keyHash) {
			return hash_table->elements[hash];
		}
	}
	return NULL;
}


bool isKeyInDict_1(unsigned int key, struct hash_table_int_key_str_value *pHashTable) {


	// Iterate the dictionary by keys
	//	char * pValue;
	struct key_value_pair_int_key_str_val *pKeyValuePair = NULL;



	pKeyValuePair = hash_retrieve_1(key, pHashTable);
	if (NULL == pKeyValuePair) {
		return false;
	}

	return true;
}



// Returns the number of keys in the dictionary
// The list of keys in the dictionary is returned as a parameter.  It will need to be freed afterwards
int keys_1(struct hash_table_int_key_str_value *pHashTable, unsigned int **ppKeys) {

	unsigned int num_keys = 0;

	*ppKeys = (unsigned int *) malloc( pHashTable->number_of_elements * sizeof(unsigned int) );

	for (unsigned int i = 0; i < pHashTable->max; i++) {
		if (NULL != pHashTable->elements[i]) {
			(*ppKeys)[num_keys] = pHashTable->elements[i]->key;
			num_keys++;
		}
	}
	return num_keys;
}


bool isKeyInDict_2(unsigned char *pKey, int lenKey, struct hash_table_str_key_int_value *pHashTable) {


	// Iterate the dictionary by keys
	//	char * pValue;
	struct key_value_pair_str_key_int_val *pKeyValuePair = NULL;
	unsigned int keyHash;



	keyHash = FNV_hash(pKey, lenKey);

	pKeyValuePair = hash_retrieve_2(keyHash, pHashTable);
	if (NULL == pKeyValuePair) {
		return false;
	}

	return true;
}


// Returns the number of keys in the dictionary
// The list of keys in the dictionary is returned as a parameter.  It will need to be freed afterwards
// in this case, we return the keyHash.  Given the key hash, the key and value can be found.
int keys_2(struct hash_table_str_key_int_value *pHashTable, int **ppKeys) {

	int num_keys = 0;

	*ppKeys = (int *)malloc(pHashTable->number_of_elements * sizeof(int));

	for (int i = 0; i < pHashTable->max; i++) {
		if (NULL != pHashTable->elements[i]) {
			(*ppKeys)[num_keys] = pHashTable->elements[i]->keyHash;
			num_keys++;
		}
	}
	return num_keys;
}




// The dictionary will need to be freed afterwards
// For use with dictionary's which are
// key int
// value string
int allocate_the_dictionary_intKey_strValue(struct hash_table_int_key_str_value *pHashTable) {


	// Allocate the hash table slots
	pHashTable->elements = (struct key_value_pair_int_key_str_val **) malloc(pHashTable->max * sizeof(struct key_value_pair_int_key_str_val));  // allocate max number of key_value_pair entries
	for (unsigned int i = 0; i < pHashTable->max; i++) {
		pHashTable->elements[i] = NULL;
	}

	return RC_OK;
}


// The dictionary will need to be freed afterwards
// For use with dictionary's which are
// key string
// value int
int allocate_the_dictionary_strKey_intValue(struct hash_table_str_key_int_value *pHashTable) {


	// Allocate the hash table slots
	pHashTable->elements = (struct key_value_pair_str_key_int_val **) malloc(pHashTable->max * sizeof(struct key_value_pair_str_key_int_val));  // allocate max number of key_value_pair entries
	for (int i = 0; i < pHashTable->max; i++) {
		pHashTable->elements[i] = NULL;
	}

	return RC_OK;
}



// This will make a dictionary entry where
//   o  key is an int
//   o  value is a character buffer
//   o  valueLen is len of char buffer
//
// The buffer in the key_value_pair will need to be freed afterwards
int make_dict_entry_1(unsigned int a_key, unsigned char * pValue, int valueLen,  struct key_value_pair_int_key_str_val *pKeyValuePair) {


	// alloc the buffer to hold the string
	pKeyValuePair->pValue = (unsigned char *) malloc(valueLen); // do not add one for the null terminator byte because we are using one
	if (NULL == pKeyValuePair->pValue) {
		printf("Failed to allocate the buffer for the dictionary string value.");
		return RC_ERROR;
	}
	// This secure version of copy seems to require the buffer be
	// freed from the same pointer. Yes, really.
//	strncpy_s(pMyStruct->pValue, sizeof(len+1), buffer, _TRUNCATE);
	memcpy(pKeyValuePair->pValue, pValue, valueLen);
	pKeyValuePair->key = a_key;
	pKeyValuePair->valueLen = valueLen;

	return RC_OK;
}



// This will make a dictionary entry where
//   o  key is a string
//   o  keyLen is len of string. No null zero.
//   o  value is a int
//   o  keyHash is an int hash of the string key
//
// The buffer in the key_value_pair will need to be freed afterwards
int make_dict_entry_2(unsigned char *pKey, int lenKey, int value, struct key_value_pair_str_key_int_val *pKeyValuePair) {


	// alloc the buffer to hold the string
	pKeyValuePair->pKey = (unsigned char *)malloc(lenKey); // do not add one for the null terminator byte.  We can no longer depend on byte seq as a string
	if (NULL == pKeyValuePair->pKey) {
		printf("Failed to allocate the buffer for the dictionary string key.");
		return RC_ERROR;
	}
	// This secure version of copy seems to require the buffer be
	// freed from the same pointer. Yes, really.
	//	strncpy_s(pMyStruct->pValue, sizeof(len+1), buffer, _TRUNCATE);
	memcpy(pKeyValuePair->pKey, pKey, lenKey);
	pKeyValuePair->value = value;
	pKeyValuePair->keyLen = lenKey;

	pKeyValuePair->keyHash = FNV_hash(pKeyValuePair->pKey, lenKey);
	//printf("hash is %d\n", pMyStruct->keyHash);



	return RC_OK;
}


// Assumes the hash table has already been allocated.
// This is for type 1 dictionarys
int add_key_val_pair_to_dict_1(struct hash_table_int_key_str_value *pHashTable, unsigned int key, unsigned char *pValue, int valueLen) {

	int rc;
	struct key_value_pair_int_key_str_val *pKeyValuePair;

	if (NULL == pHashTable) {
		printf("Hash table is null.\n");
		return RC_ERROR;
	}

	// Allocate the dictionary key value pair struct
	pKeyValuePair = (struct key_value_pair_int_key_str_val *) malloc(sizeof(struct key_value_pair_int_key_str_val));
	if (NULL == pKeyValuePair) {
		printf("Failed to allocate key value pair struct.\n");
		return RC_ERROR;
	}


	rc = make_dict_entry_1(key, pValue, valueLen, pKeyValuePair);  // a_hash_table[1221] = "abba"
	if (RC_ERROR == rc) {
		printf("Failed to add buff to key value pair struct.\n");
		return RC_ERROR;
	}


	rc = hash_insert_1(pKeyValuePair, pHashTable);
	if (RC_ERROR == rc) {
		printf("insert has failed!\n");
		return RC_ERROR;
	}

	return RC_OK;
}




// Assumes the hash table has already been allocated.
// This is for type 2 dictionarys
// This dictionary has
// o key string
// o value int
// o hash of string as int
int add_key_val_pair_to_dict_2(struct hash_table_str_key_int_value *pHashTable, unsigned char *pKey, int lenKey, int value) {

	int rc;
	struct key_value_pair_str_key_int_val *pKeyValuePair;

	if (NULL == pHashTable) {
		printf("Hash table is null.\n");
		return RC_ERROR;
	}

	// Allocate the dictionary key value pair struct
	pKeyValuePair = (struct key_value_pair_str_key_int_val *) malloc(sizeof(struct key_value_pair_str_key_int_val));
	if (NULL == pKeyValuePair) {
		printf("Failed to allocate key value pair struct.\n");
		return RC_ERROR;
	}


	rc = make_dict_entry_2(pKey, lenKey, value, pKeyValuePair);  // a_hash_table[1221] = "abba"
	if (RC_ERROR == rc) {
		printf("Failed to add buff to key value pair struct.\n");
		return RC_ERROR;
	}


	rc = hash_insert_2(pKeyValuePair, pHashTable);
	if (RC_ERROR == rc) {
		printf("hash insert 2 has failed!\n");
		return RC_ERROR;
	}

	return RC_OK;
}



void dump_hash_table_1(struct hash_table_int_key_str_value *pHashTable) {

	// Iterate the dictionary by keys
	unsigned char *pValue = NULL;
	struct key_value_pair_int_key_str_val *pKeyValuePair;
	unsigned int *pKeyList;
	unsigned int num_keys;

	printf("i\tKey\tValue\n");
	printf("-----------------------------\n");
	num_keys = keys_1(pHashTable, &pKeyList);
	for (unsigned int i = 0; i < num_keys; i++) {
		pKeyValuePair = hash_retrieve_1(pKeyList[i], pHashTable);
		pValue = (unsigned char *) realloc(pValue, pKeyValuePair->valueLen + 1);
		memcpy(pValue, pKeyValuePair->pValue, pKeyValuePair->valueLen);
		pValue[pKeyValuePair->valueLen] = 0;
		printf("%d\t%d\t%s\n", i, pKeyList[i], pValue);
	}

	// free the value pointer
	free(pValue);

	// Free the key list
	free(pKeyList);

}


void dump_hash_table_2(struct hash_table_str_key_int_value *pHashTable) {

	// Iterate the dictionary by keys
//	char * pValue;
	struct key_value_pair_str_key_int_val *pKeyValuePair;
	int *pKeyHashList;
	int num_keys;
	// the key value pair details
	unsigned char *pKey;
	int keyHash;
	int value;

	printf("i\tKey\tKey Hash\tValue\n");
	printf("-----------------------------------\n");
	num_keys = keys_2(pHashTable, &pKeyHashList);
	for (int i = 0; i < num_keys; i++) {
		pKeyValuePair = hash_retrieve_2(pKeyHashList[i], pHashTable);
		pKey = pKeyValuePair->pKey;
		keyHash = pKeyValuePair->keyHash;
		value = pKeyValuePair->value;
		printf("%d \t %s \t %d \t %d\n", i, pKey, keyHash, value);
	}

	// Free the key list
	free(pKeyHashList);

}



void free_hash_table_2(struct hash_table_str_key_int_value *pHashTable) {

	// Iterate the dictionary by keys
	//	char * pValue;
	struct key_value_pair_str_key_int_val *pKeyValuePair;
	int *pKeyHashList;
	int num_keys;
	// the key value pair details
	unsigned char *pKey;
	int keyHash;
	int value;

//	printf("i\tKey\tKey Hash\tValue\n");
//	printf("-----------------------------------\n");
	num_keys = keys_2(pHashTable, &pKeyHashList);
	for (int i = 0; i < num_keys; i++) {
		pKeyValuePair = hash_retrieve_2(pKeyHashList[i], pHashTable);
		pKey = pKeyValuePair->pKey;
		keyHash = pKeyValuePair->keyHash;
		value = pKeyValuePair->value;
		free(pKey);
		free(pKeyValuePair);
		pKeyValuePair = NULL;

	}

	// Free the key list
	free(pKeyHashList);

	// Free the HashTable slots
	free(pHashTable->elements);
	// Free the hash table itself
	free(pHashTable);

}




void free_hash_table_1(struct hash_table_int_key_str_value *pHashTable) {

	// Iterate the dictionary by keys
	//	char * pValue;
	struct key_value_pair_int_key_str_val *pKeyValuePair;
	unsigned int *pKeyHashList;
	unsigned int num_keys;
	// the key value pair details
	unsigned int key;
	unsigned int valueLen;
	unsigned char *pchValue;

//	printf("i\tKey\tValue\n");
//	printf("-----------------------------------\n");
	num_keys = keys_1(pHashTable, &pKeyHashList);
//	printf("num_keys = %d\n", num_keys);
	for (unsigned int i = 0; i < num_keys; i++) {

		pKeyValuePair = hash_retrieve_1(pKeyHashList[i], pHashTable);

		key = pKeyValuePair->key;
		pchValue = pKeyValuePair->pValue;
		valueLen = pKeyValuePair->valueLen;
//		printf("i=%d key=%d hashlist=%d valueLen=%d\n", i, key, pKeyHashList[i], valueLen);
		free(pchValue);  // this fails on the bigger input file
		pchValue = NULL;
		free(pKeyValuePair); // this fails on the bigger input file
		pKeyValuePair = NULL;

	}

	// Free the key list
	free(pKeyHashList);

	// Free the HashTable slots
	free(pHashTable->elements);
	// Free the HashTable slots
	free(pHashTable);

}





// Getting the value for a string key needs a conveniece function
// The return code determines if the key was found.
int get_val_for_key_2(struct hash_table_str_key_int_value *pDict, unsigned char *pchKey, int lenKey, int *piResultValue) {

	unsigned int keyHash;
	struct key_value_pair_str_key_int_val *pKeyValuePair = NULL;

	keyHash = FNV_hash(pchKey, lenKey);

	pKeyValuePair = hash_retrieve_2(keyHash, pDict);
	if (NULL == pKeyValuePair) {
		printf("w was not in dictionary.  it shuld have been\n");
		return RC_ERROR;  // This should never hit
	}

	// extract the symbol from the key value pair
	*piResultValue = pKeyValuePair->value;

	return RC_OK;
}

// Getting the value for a int key needs a conveniece function
// The return code determines if the key was found.
int get_val_for_key_1(struct hash_table_int_key_str_value *pDict, unsigned int key, unsigned char **ppchResultValue, int *pResultLen) {

	struct key_value_pair_int_key_str_val *pKeyValuePair = NULL;


	pKeyValuePair = hash_retrieve_1(key, pDict);
	if (NULL == pKeyValuePair) {
		printf("key was not in dictionary.  it shuld have been\n");
		return RC_ERROR;  // This should never hit
	}

	// extract the symbol from the key value pair
	*ppchResultValue = pKeyValuePair->pValue;
	*pResultLen = pKeyValuePair->valueLen;

	return RC_OK;

}
