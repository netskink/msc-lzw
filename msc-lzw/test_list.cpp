#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "main.h"
#include "list.h"


#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>	




#ifdef TEST_LIST

int test_list_of_numbers_1(void) {

	list_t *pListOfNumbers;
	int pSomeNumbers[] = { 3, 7, 8, 1 , 2, 6, 5, 9, 10, 4 };
	int num_numbers = 10;
	//	int pSomeNumbers[] = { 1, 2, 3};
	//int num_numbers = 3;
	node_t *pNode;
	int *pValue;
	int *pExpectedValue;
	int i;
	int iRC;

	//
	// Demo the list with a list of integers using append method
	//


	// Create a linked list of integers.
	pListOfNumbers = init_list(&compare_ints);
	if (NULL == pListOfNumbers) {
		printf("Failed ot init list.\n");
	}

	// add a few numbers to the list
	for (i = 0; i<num_numbers; i++) {
		if (append(pListOfNumbers, &pSomeNumbers[i])) {
			printf("Failed to append number to list.\n");
		}
	}

	// dump the list starting with the head and verify the
	// numbers are in correct order
	printf("Dump list after it was created.\n");
	int pExpectedResults1[] = { 3, 7, 8, 1 , 2, 6, 5, 9, 10, 4 };
	//int pExpectedResults1[] = { 1, 2, 3 };

	pNode = pListOfNumbers->pFirst;
	pExpectedValue = pExpectedResults1;
	while (NULL != pNode) {
		pValue = ( (int *) pNode->pData );
		printf("List item as number is %d.\n", *( (int *) pNode->pData ) );
		if (*pValue != *pExpectedValue) {
			printf("expected result does not match returned value. Exit\n");
			return RC_ERROR;
		}
		pNode = pNode->pNext;
		pExpectedValue++;
	}


	// Test the reverse method
	pListOfNumbers = reverse(pListOfNumbers);
	if (NULL == pListOfNumbers) {
		printf("An error occured when reversing the list.\n");
		return RC_ERROR;
	}

	// dump the list starting with the head. And verify
	// the numbers are properly reversed.
	printf("Dump list after it was reversed.\n");
	int pExpectedResults2[] = { 4, 10, 9, 5, 6, 2, 1, 8, 7, 3 };

	pNode = pListOfNumbers->pFirst;
	pExpectedValue = pExpectedResults2;
	while (NULL != pNode) {
		pValue = ( (int *) pNode->pData );
		printf("List item as number is %d.\n", *( (int *) pNode->pData ));
		if (*pValue != *pExpectedValue) {
			printf("Reversed expected result does not match returned value. Exit\n");
			return RC_ERROR;
		}
		pNode = pNode->pNext;
		pExpectedValue++;
	}



	// Test the pop method
	pValue = (int *) pop(pListOfNumbers, 0);
	printf("pop[0] pValue is %d\n", *pValue);
	if (3 != *pValue) {
		printf("expected value from pop[0] was wrong. Exiting.\n");
		return RC_ERROR;
	}
	// Dump the list after the first one was popped
	printf("Dump list after the first element was popped.\n");
	pNode = pListOfNumbers->pFirst;
	while (NULL != pNode) {
		printf("List item as number is %d.\n", *( (int *) pNode->pData ));
		pNode = pNode->pNext;
	}


	// Test the pop method again
	pValue = (int *) pop(pListOfNumbers, 2);
	printf("pop[2] pValue is %d\n", *pValue);
	if (1 != *pValue) {
		printf("expected value from pop[2] was wrong. Exiting.\n");
		return RC_ERROR;
	}
	// Dump the list after the first one was popped
	printf("Dump list after the third element was popped.\n");
	pNode = pListOfNumbers->pFirst;
	while (NULL != pNode) {
		printf("List item as number is %d.\n", *( (int *) pNode->pData ));
		pNode = pNode->pNext;
	}


	// Test the pop method for end case
	pValue = (int *) pop(pListOfNumbers, 7);
	printf("pop[7] pValue is %d\n", *pValue);
	if (4 != *pValue) {
		printf("expected value from pop[7] was wrong. Exiting.\n");
		return RC_ERROR;
	}
	// Dump the list after the first one was popped
	printf("Dump list after the last element was popped.\n");
	pNode = pListOfNumbers->pFirst;
	while (NULL != pNode) {
		printf("List item as number is %d.\n", *( (int *) pNode->pData ));
		pNode = pNode->pNext;
	}

	// free the list
	iRC = free_list(pListOfNumbers);

	return iRC;
}


// This one will allocate copies of the numbers.
int test_list_of_numbers_2(void) {

	list_t *pListOfNumbers;
	int pSomeNumbers[] = { 3, 7, 8, 1 , 2, 6, 5, 9, 10, 4 };
	node_t *pNode;
	int *pValue;
	int i;
	int iRC;

	//
	// Demo the list with a list of integers using append method
	//


	// Create a linked list of integers.
	pListOfNumbers = init_list(&compare_ints);
	if (NULL == pListOfNumbers) {
		printf("Failed ot init list.\n");
	}

	// add a few numbers to the list
	for (i = 0; i<10; i++) {
		if (append2(pListOfNumbers, &pSomeNumbers[i], eMALLOC_INT)) {
			printf("Failed to append number to list.\n");
		}
	}

	// dump the list starting with the head
	printf("Dump list after it was created.\n");
	pNode = pListOfNumbers->pFirst;
	while (NULL != pNode) {
		printf("List item as number is %d.\n", *( (int *) pNode->pData ));
		pNode = pNode->pNext;
	}

	// Test the pop method
	pValue = (int *) pop(pListOfNumbers, 0);
	printf("pop[0] pValue is %d\n", *pValue);
	if (3 != *pValue) {
		printf("expected value from pop[0] was wrong. Exiting.\n");
		return RC_ERROR;
	}
	// pop does not free the data for the value.  We need to do that since we
	// added it with append2()
	free(pValue);

	// Dump the list after the first one was popped
	printf("Dump list after the first element was popped.\n");
	pNode = pListOfNumbers->pFirst;
	while (NULL != pNode) {
		printf("List item as number is %d.\n", *( (int *) pNode->pData ));
		pNode = pNode->pNext;
	}


	// Test the pop method again
	pValue = (int *) pop(pListOfNumbers, 2);
	printf("pop[2] pValue is %d\n", *pValue);
	if (1 != *pValue) {
		printf("expected value from pop[2] was wrong. Exiting.\n");
		return RC_ERROR;
	}
	// pop does not free the data for the value.  We need to do that since we
	// added it with append2()
	free(pValue);

	// Dump the list after the first one was popped
	printf("Dump list after the third element was popped.\n");
	pNode = pListOfNumbers->pFirst;
	while (NULL != pNode) {
		printf("List item as number is %d.\n", *( (int *) pNode->pData ));
		pNode = pNode->pNext;
	}


	// Test the pop method for end case
	pValue = (int *) pop(pListOfNumbers, 7);
	printf("pop[7] pValue is %d\n", *pValue);
	if (4 != *pValue) {
		printf("expected value from pop[7] was wrong. Exiting.\n");
		return RC_ERROR;
	}
	// pop does not free the data for the value.  We need to do that since we
	// added it with append2()
	free(pValue);

	// Dump the list after the first one was popped
	printf("Dump list after the last element was popped.\n");
	pNode = pListOfNumbers->pFirst;
	while (NULL != pNode) {
		printf("List item as number is %d.\n", *( (int *) pNode->pData ));
		pNode = pNode->pNext;
	}

	// free the list
	iRC = free_list(pListOfNumbers);

	return iRC;

}



int demo_list_of_numbers(void) {

	int iRC;
	 
	iRC = test_list_of_numbers_1();
	if (iRC == RC_ERROR) {
		printf("test list of numbers 1 has failed!\n");
		return RC_ERROR;
	}

	iRC = test_list_of_numbers_2();
	if (iRC == RC_ERROR) {
		printf("test list of numbers 2 has failed!\n");
		return RC_ERROR;
	}





	return RC_OK;
}


int main(int argc, char *argv[]) {


	int iRC;

//	list_t *pListOfNames;

//	char pSomeNames[10][20] = { "John", "Fred", "Davis" , "Adam", "Mike", "Larry", "Charlie", "Baker" , "Dan", "Edwin" };


	iRC = EXIT_SUCCESS;

	if (argc != 1) {
		// one arg fprintf(stderr, "Usage: %s <file>\n", argv[0]);
		fprintf(stderr, "Usage: %s \n", argv[0]);
		iRC = EXIT_FAILURE;
		exit(iRC);
	}

	iRC = demo_list_of_numbers();



	printf("TESTS COMPLETE OK!\n");



	_CrtDumpMemoryLeaks();

	return iRC;

}

#endif