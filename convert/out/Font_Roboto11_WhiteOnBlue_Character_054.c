
// created yyyy-mm-dd 

#include "assets.h" 

const short int Font_Roboto11_WhiteOnBlue_Character_054_x = 9;
const short int Font_Roboto11_WhiteOnBlue_Character_054_y = 22;

const unsigned short Font_Roboto11_WhiteOnBlue_Character_054[] = {
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1e2d, 
0xffbf, 0xffff, 0xffff, 0xffff, 0x1d75, 0x1d2d, 0x1d2d, 0x9e2d, 
0xffdf, 0x1eff, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x7f97, 0xffff, 0x1d75, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1e2d, 0xffbf, 0x1eff, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x9e2d, 0xffdf, 0x1fff, 0xffbf, 0xffff, 0xffff, 0x1eff, 
0x1d2d, 0x1d2d, 0x9e2d, 0xffdf, 0xffff, 0x1d75, 0x1d2d, 0x1f2e, 
0xffff, 0x9dde, 0x1d2d, 0x9e2d, 0xffdf, 0x1eff, 0x1d2d, 0x1d2d, 
0x1d2d, 0x7f97, 0xffff, 0x1d75, 0x9e2d, 0xffdf, 0x1eff, 0x1d2d, 
0x1d2d, 0x1d2d, 0x7f97, 0xffff, 0x1d75, 0x1d2d, 0x7f97, 0x7eff, 
0x1d55, 0x1d2d, 0x1d2d, 0x7f97, 0x7eff, 0x1d55, 0x1d2d, 0x1f2e, 
0xffff, 0x9dde, 0x1d2d, 0x1f2e, 0xffff, 0x9dde, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1f2e, 0xffff, 0xffff, 0xffff, 0x9dde, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, };


const image_t imageFont_Roboto11_WhiteOnBlue_Character_054 = {
	Font_Roboto11_WhiteOnBlue_Character_054_x,
	Font_Roboto11_WhiteOnBlue_Character_054_y,
	Font_Roboto11_WhiteOnBlue_Character_054
};


