// msc-lzw.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <string.h>
#include "main.h"
#include "dictionary.h"

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>	


#ifdef TEST_DICTIONARY





int check_demo_dict_1(struct hash_table_int_key_str_value *pHashTable) {

	// ensure the dictionary is working properly
	unsigned char * pValue;
	struct key_value_pair_int_key_str_val *pMyStruct;
	unsigned int *pKeyList;
	int num_keys;
	int key;

	printf("testing key value pairs\n");
	printf("-----------------------------\n");
	num_keys = keys_1(pHashTable, &pKeyList);
	for (int i = 0; i < num_keys; i++) {
		pMyStruct = hash_retrieve_1(pKeyList[i], pHashTable);
		key = pKeyList[i];
		pValue = pMyStruct->pValue;

		switch (key) {
		case 1221: 
			if (0 == memcmp(pValue, "abba", 4)) {
				printf("key value pair correct\n");
			} else {
				printf("Error: key %d has incorrect value.\n", key);
				return RC_ERROR;
			}

			break;

		case 1122:
			if (0 == memcmp(pValue, "aabb", 4)) {
				printf("key value pair correct\n");
			}
			else {
				printf("Error: key %d has incorrect value.\n", key);
				return RC_ERROR;
			}

			break;

		case 1212: 
			if (0 == memcmp(pValue, "abab", 4)) {
				printf("key value pair correct\n");
			}
			else {
				printf("Error: key %d has incorrect value.\n", key);
				return RC_ERROR;
			}
		
		break;

		case 2121: 
			if (0 == memcmp(pValue, "baba", 4)) {
				printf("key value pair correct\n");
			}
			else {
				printf("Error: key %d has incorrect value.\n", key);
				return RC_ERROR;
			}
		
			break;

		case 2211: 
			if (0 == memcmp(pValue, "bbaa", 4)) {
				printf("key value pair correct\n");
			}
			else {
				printf("Error: key %d has incorrect value.\n", key);
				return RC_ERROR;
			}

		   break;


		case 2112: 
			if (0 == memcmp(pValue, "baab", 4)) {
				printf("key value pair correct\n");
			}
			else {
				printf("Error: key %d has incorrect value.\n", key);
				return RC_ERROR;
			}

		   break;

		case 01234: {
			char pchBuf[] = { 0, 1, 2, 3, 4 };

			if (0 == memcmp(pValue, pchBuf, 5)) {
				printf("key value pair correct\n");
			} else {
				printf("Error: key %d has incorrect value.\n", key);
				return RC_ERROR;
			}
		}
			break;


		case 500: {
			char pchBuf[] = { 0, 0, 0, 0, 0 };

			if (0 == memcmp(pValue, pchBuf, 5)) {
				printf("key value pair correct\n");
			} else {
				printf("Error: key %d has incorrect value.\n", key);
				return RC_ERROR;
			}
		}
					break;

		case 100: {
			char pchBuf[] = { 0, 0, 1, 0, 0 };

			if (0 == memcmp(pValue, pchBuf, 5)) {
				printf("key value pair correct\n");
			} else {
				printf("Error: key %d has incorrect value.\n", key);
				return RC_ERROR;
			}
		}
				  break;



		default:
			printf("Error: key %d is not in dictionary.\n", key);
			return RC_ERROR;

		}

	}

	// Free the key list
	free(pKeyList);

	return RC_OK;
}



// Demos the dictionary which is of type
// key int
// value string
int demo_dictionary_type_1(void) {

	int rc;
	unsigned int i;


	printf("\ndemo dictionary 1 begin\n");


	struct hash_table_int_key_str_value a_hash_table;
	a_hash_table.max = 20;   // The dictionary can hold at most 20 entries.
	a_hash_table.number_of_elements = 0;  // The intial dictionary has 0 entries.
	allocate_the_dictionary_intKey_strValue(&a_hash_table);

	/// These are simple ascii string additiona.
	rc = add_key_val_pair_to_dict_1(&a_hash_table, 1221, (unsigned char *)"abba", 4);
	if (RC_ERROR == rc) {
		printf("insert has failed!\n");
		return RC_ERROR;
	}
	rc = add_key_val_pair_to_dict_1(&a_hash_table, 2211, ( unsigned char *) "bbaa", 4);
	if (RC_ERROR == rc) {
		printf("insert has failed!\n");
		return RC_ERROR;
	}
	rc = add_key_val_pair_to_dict_1(&a_hash_table, 1122, ( unsigned char * ) "aabb", 4);
	if (RC_ERROR == rc) {
		printf("insert has failed!\n");
		return RC_ERROR;
	}
	rc = add_key_val_pair_to_dict_1(&a_hash_table, 2112, ( unsigned char * ) "baab", 4);
	if (RC_ERROR == rc) {
		printf("insert has failed!\n");
		return RC_ERROR;
	}
	rc = add_key_val_pair_to_dict_1(&a_hash_table, 1212, ( unsigned char * ) "abab", 4);
	if (RC_ERROR == rc) {
		printf("insert has failed!\n");
		return RC_ERROR;
	}
	rc = add_key_val_pair_to_dict_1(&a_hash_table, 2121, ( unsigned char * ) "baba", 4);
	if (RC_ERROR == rc) {
		printf("insert has failed!\n");
		return RC_ERROR;
	}
	// test some more complex binary file sequences.  See if they are handled correctly
	unsigned char pchBuf1[] = { 0, 1, 2, 3, 4 };
	rc = add_key_val_pair_to_dict_1(&a_hash_table, 01234, pchBuf1, 5);
	if (RC_ERROR == rc) {
		printf("insert has failed!\n");
		return RC_ERROR;
	}

	unsigned char pchBuf2[] = { 0, 0, 0, 0, 0 };
	rc = add_key_val_pair_to_dict_1(&a_hash_table, 500, pchBuf2, 5);
	if (RC_ERROR == rc) {
		printf("insert has failed!\n");
		return RC_ERROR;
	}

	unsigned char pchBuf3[] = { 0, 0, 1, 0, 0 };
	rc = add_key_val_pair_to_dict_1(&a_hash_table, 100, pchBuf3, 5);
	if (RC_ERROR == rc) {
		printf("insert has failed!\n");
		return RC_ERROR;
	}

	// Iterate the dictionary by keys
	dump_hash_table_1(&a_hash_table);

	// Check the keys for proper values
	rc = check_demo_dict_1(&a_hash_table);
	if (RC_ERROR == rc) {
		printf("key value pair does not match expected values.\n");
		return RC_ERROR;
	}


	// Free the individual slots
	for (i = 0; i < a_hash_table.max; i++) {
		// all that he could see was babylon
		if (NULL != a_hash_table.elements[i]) {
			//printf("i=%d\n", i);
			free(a_hash_table.elements[i]->pValue);  // free the buffer in the struct
			free(a_hash_table.elements[i]);  // free the key_value_pair entry
			a_hash_table.elements[i] = NULL;  // set the element pointer to unused
		}
	}


	printf("\ndemo dictionary 1 complete\n");

	// Free the overall dictionary
	free(a_hash_table.elements);

	return 0;

}




int check_demo_dict_2(struct hash_table_str_key_int_value *pHashTable) {

	// ensure the dictionary is working properly
	unsigned char * pKey;
	struct key_value_pair_str_key_int_val *pKeyValuePair;
	int *pKeyHashList;
	int num_keys;
	int value;
	int keyHash;

	printf("testing key value pairs\n");
	printf("-----------------------------\n");
	num_keys = keys_2(pHashTable, &pKeyHashList);
	for (int i = 0; i < num_keys; i++) {
		pKeyValuePair = hash_retrieve_2(pKeyHashList[i], pHashTable);
		keyHash = pKeyHashList[i];
		pKey = pKeyValuePair->pKey;
		value = pKeyValuePair->value;

		switch (value) {
		case 1221:
			if (0 == memcmp(pKey, "abba", 4)) {
				printf("key value pair correct\n");
			}
			else {
				printf("Error: key %d has incorrect value.\n", value);
				return RC_ERROR;
			}

			break;

		case 1122:
			if (0 == memcmp(pKey, "aabb", 4)) {
				printf("key value pair correct\n");
			}
			else {
				printf("Error: key %d has incorrect value.\n", value);
				return RC_ERROR;
			}

			break;

		case 1212:
			if (0 == memcmp(pKey, "abab", 4)) {
				printf("key value pair correct\n");
			}
			else {
				printf("Error: key %d has incorrect value.\n", value);
				return RC_ERROR;
			}

			break;

		case 2121:
			if (0 == memcmp(pKey, "baba", 4)) {
				printf("key value pair correct\n");
			}
			else {
				printf("Error: key %d has incorrect value.\n", value);
				return RC_ERROR;
			}

			break;

		case 2211:
			if (0 == memcmp(pKey, "bbaa", 4)) {
				printf("key value pair correct\n");
			}
			else {
				printf("Error: key %d has incorrect value.\n", value);
				return RC_ERROR;
			}

			break;


		case 2112:
			if (0 == memcmp(pKey, "baab", 4)) {
				printf("key value pair correct\n");
			}
			else {
				printf("Error: key %d has incorrect value.\n", value);
				return RC_ERROR;
			}

			break;

		case 01234: {
			char pchBuf[] = { 0, 1, 2, 3, 4 };

			if (0 == memcmp(pKey, pchBuf, 5)) {
				printf("key value pair correct\n");
			} else {
				printf("Error: key %d has incorrect value.\n", value);
				return RC_ERROR;
			}
		}
					break;


		case 500: {
			char pchBuf[] = { 0, 0, 0, 0, 0 };

			if (0 == memcmp(pKey, pchBuf, 5)) {
				printf("key value pair correct\n");
			} else {
				printf("Error: key %d has incorrect value.\n", value);
				return RC_ERROR;
			}
		}
					break;


		case 100: {
			char pchBuf[] = { 0, 0, 1, 0, 0 };

			if (0 == memcmp(pKey, pchBuf, 5)) {
				printf("key value pair correct\n");
			} else {
				printf("Error: key %d has incorrect value.\n", value);
				return RC_ERROR;
			}
		}
				  break;


		default:
			printf("Error: value %d is not in dictionary.\n", value);
			return RC_ERROR;

		}

	}

	// Free the key list
	free(pKeyHashList);

	return RC_OK;
}




// Demos the dictionary which is of type
// key string
// value int
int demo_dictionary_type_2(void) {

	int rc;
	int i;



	printf("\ndemo dictionary 2 begin\n");


	struct hash_table_str_key_int_value a_hash_table;
	a_hash_table.max = 20;   // The dictionary can hold at most 20 entries.
	a_hash_table.number_of_elements = 0;  // The intial dictionary has 0 entries.
	allocate_the_dictionary_strKey_intValue(&a_hash_table);

	rc = add_key_val_pair_to_dict_2(&a_hash_table, (unsigned char *) "abba", 4, 1221);
	if (RC_ERROR == rc) {
		printf("insert has failed!\n");
		return RC_ERROR;
	}
	rc = add_key_val_pair_to_dict_2(&a_hash_table, ( unsigned char * ) "bbaa", 4, 2211);
	if (RC_ERROR == rc) {
		printf("insert has failed!\n");
		return RC_ERROR;
	}
	rc = add_key_val_pair_to_dict_2(&a_hash_table, ( unsigned char * ) "aabb", 4, 1122);
	if (RC_ERROR == rc) {
		printf("insert has failed!\n");
		return RC_ERROR;
	}
	rc = add_key_val_pair_to_dict_2(&a_hash_table, ( unsigned char * ) "baab", 4, 2112);
	if (RC_ERROR == rc) {
		printf("insert has failed!\n");
		return RC_ERROR;
	}
	rc = add_key_val_pair_to_dict_2(&a_hash_table, ( unsigned char * ) "abab", 4, 1212);
	if (RC_ERROR == rc) {
		printf("insert has failed!\n");
		return RC_ERROR;
	}
	rc = add_key_val_pair_to_dict_2(&a_hash_table, ( unsigned char * ) "baba", 4, 2121);
	if (RC_ERROR == rc) {
		printf("insert has failed!\n");
		return RC_ERROR;
	}
	// test some more complex binary file sequences.  See if they are handled correctly
	unsigned char pchBuf1[] = { 0, 1, 2, 3, 4 };
	rc = add_key_val_pair_to_dict_2(&a_hash_table, pchBuf1, 5, 01234);
	if (RC_ERROR == rc) {
		printf("insert has failed!\n");
		return RC_ERROR;
	}
	unsigned char pchBuf2[] = { 0, 0, 0, 0, 0 };
	rc = add_key_val_pair_to_dict_2(&a_hash_table, pchBuf2, 5, 500);
	if (RC_ERROR == rc) {
		printf("insert has failed!\n");
		return RC_ERROR;
	}

	unsigned char pchBuf3[] = { 0, 0, 1, 0, 0 };
	rc = add_key_val_pair_to_dict_2(&a_hash_table, pchBuf3, 5, 100);
	if (RC_ERROR == rc) {
		printf("insert has failed!\n");
		return RC_ERROR;
	}


	//// Iterate the dictionary by keys
	dump_hash_table_2(&a_hash_table);



	// Check the keys for proper values
	rc = check_demo_dict_2(&a_hash_table);
	if (RC_ERROR == rc) {
		printf("key value pair does not match expected values.\n");
		return RC_ERROR;
	}





	// Free the individual slots
	for (i = 0; i < a_hash_table.max; i++) {
		// all that he could see was babylon
		if (NULL != a_hash_table.elements[i]) {
			//printf("i=%d\n", i);
			free(a_hash_table.elements[i]->pKey);  // free the buffer in the struct
			free(a_hash_table.elements[i]);  // free the key_value_pair entry
			a_hash_table.elements[i] = NULL;  // set the element pointer to unused
		}
	}



	printf("\ndemo dictionary 2 complete\n");


	//// Free the overall dictionary
	free(a_hash_table.elements);


	return 0;


}





int main(int argc, char *argv[]) {


	int iRC;


	iRC = EXIT_SUCCESS;

	if (argc != 1) {
		// one arg fprintf(stderr, "Usage: %s <file>\n", argv[0]);
		fprintf(stderr, "Usage: %s \n", argv[0]);
		iRC = EXIT_FAILURE;
		exit(iRC);
	}

	iRC = demo_dictionary_type_1();
	if (iRC == RC_ERROR) {
		printf("demo dictionary 1 has failed!\n");
		return RC_ERROR;
	}
	iRC = demo_dictionary_type_2();
	if (iRC == RC_ERROR) {
		printf("demo dictionary 2 has failed!\n");
		return RC_ERROR;
	}

	printf("TESTS COMPLETE OK!\n");

	_CrtDumpMemoryLeaks();


	return iRC;

}

#endif