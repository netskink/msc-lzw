
// created yyyy-mm-dd 

#ifndef Font_Roboto_40_white_on_gray_248_H
#define Font_Roboto_40_white_on_gray_248_H

#include "assets.h" 

extern const short int Font_Roboto_40_white_on_gray_248_x;
extern const short int Font_Roboto_40_white_on_gray_248_y;
extern const unsigned short Font_Roboto_40_white_on_gray_248[];
extern const image_t imageFont_Roboto_40_white_on_gray_248;


#endif


