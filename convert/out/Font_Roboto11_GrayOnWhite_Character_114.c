
// created yyyy-mm-dd 

#include "assets.h" 

const short int Font_Roboto11_GrayOnWhite_Character_114_x = 6;
const short int Font_Roboto11_GrayOnWhite_Character_114_y = 16;

const unsigned short Font_Roboto11_GrayOnWhite_Character_114[] = {
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x2a7a, 0x1134, 
0x2a7a, 0x2d32, 0x1f9f, 0xffff, 0x2a7a, 0x3132, 0xffbf, 0xffff, 
0xffff, 0xffff, 0x2a7a, 0x1834, 0xffff, 0xffff, 0xffff, 0xffff, 
0x2a7a, 0x1834, 0xffff, 0xffff, 0xffff, 0xffff, 0x2a7a, 0x1834, 
0xffff, 0xffff, 0xffff, 0xffff, 0x2a7a, 0x1834, 0xffff, 0xffff, 
0xffff, 0xffff, 0x2a7a, 0x1834, 0xffff, 0xffff, 0xffff, 0xffff, 
0x2a7a, 0x1834, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
};


const image_t imageFont_Roboto11_GrayOnWhite_Character_114 = {
	Font_Roboto11_GrayOnWhite_Character_114_x,
	Font_Roboto11_GrayOnWhite_Character_114_y,
	Font_Roboto11_GrayOnWhite_Character_114
};


