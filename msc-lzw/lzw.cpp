
#include "stdafx.h"

#include <string.h>
#include <stdio.h>
#include "main.h"
#include "dictionary.h"
#include "list.h"
#include "lzw.h"
#include "utils.h"

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>	







// Builds a dictionary
// The dictionary needs to be freed afterwards
// Builds a dictionary of form:
// key is int
// value is string
struct hash_table_int_key_str_value *build_initial_dictionary_intKey_strValue(void) {

	int rc;
	unsigned char value;

	// Build the initial dictionary
	struct hash_table_int_key_str_value *pHashTable;
	pHashTable = (struct hash_table_int_key_str_value *) malloc(sizeof(struct hash_table_int_key_str_value));


	pHashTable->max = max_dict_size;   // The dictionary can hold at most ?K entries.
	pHashTable->number_of_elements = 0;  // The intial dictionary has 0 entries.
	allocate_the_dictionary_intKey_strValue(pHashTable);

	for (int i = 0; i < 256; i++) {

		value = i;
		rc = add_key_val_pair_to_dict_1(pHashTable, i, &value, 1);
		if (RC_ERROR == rc) {
			printf("insert has failed!\n");
			return NULL;
		}

	}

	//dump_hash_table_1(pHashTable);

	return  pHashTable;
}



struct hash_table_str_key_int_value *build_initial_dictionary_strKey_intValue(void) {

	int rc;
	unsigned char key; 

	// Build the initial dictionary
	struct hash_table_str_key_int_value *pHashTable;
	pHashTable = (struct hash_table_str_key_int_value *) malloc(sizeof(struct hash_table_str_key_int_value));


	pHashTable->max = max_dict_size;   // The dictionary can hold at most ?K entries.
	pHashTable->number_of_elements = 0;  // The intial dictionary has 0 entries.
	allocate_the_dictionary_strKey_intValue(pHashTable);

	for (int i = 0; i < 256; i++) {

		key = i;
		rc = add_key_val_pair_to_dict_2(pHashTable, &key, 1, i);
		if (RC_ERROR == rc) {
			printf("insert has failed!\n");
			return NULL;
		}

	}

	//dump_hash_table_2(pHashTable);

	return  pHashTable;
}


// result is a list of ints
// dictionary
//  o key is of type string
//  o value is of type int
//
//def compress(uncompressed) :
//	"""Compress a string to a list of output symbols."""
//
//	# Build the dictionary.
//	dict_size = 256
//	dictionary = dict((chr(i), i) for i in xrange(dict_size))
//	# in Python 3: dictionary = { chr(i) : i for i in range(dict_size) }
//
//	w = ""
//	result = []
//	for c in uncompressed :
//wc = w + c
//if wc in dictionary :
//w = wc
//else :
//	result.append(dictionary[w])
//	# Add wc to the dictionary.
//	dictionary[wc] = dict_size
//	dict_size += 1
//	w = c
//
//	# Output the code for w.
//	if w:
//result.append(dictionary[w])
//return result


// result is a list of ints
 // dictionary
 //  o key is of type string
 //  o value is of type int

 // w is string
 // c is string
 // wc is string
 // result is list of ints
 // uncompressed is string
 

int build_primatives_for_compression(struct hash_table_str_key_int_value **ppHashTable, list_t **ppListOfNumbers) {


	struct hash_table_str_key_int_value *pHashTable;
	list_t *pListOfNumbers;

	//int pSomeNumbers[] = { 3, 7, 8, 1 , 2, 6, 5, 9, 10, 4 };
	//node_t *pValue = NULL;


	// Build the initial dictionary
	pHashTable = build_initial_dictionary_strKey_intValue();
	if (NULL == pHashTable) {
		return RC_ERROR;
	}



	// Create a linked list of integers.
	pListOfNumbers = init_list(&compare_ints);
	if (NULL == pListOfNumbers) {
		printf("Failed ot init list.\n");
	}

	// add a few numbers to the list
	//for (int i = 0; i<10; i++) {
	//	if (append2(pListOfNumbers, &pSomeNumbers[i], eMALLOC_INT)) {
	//		printf("Failed to append number to list.\n");
	//	}
	//}

	//// dump the list starting with the head
	//pValue = pListOfNumbers->pFirst;
	//while (NULL != pValue) {
	//	printf("List item as number is %d.\n", *((int *)pValue->pData));
	//	pValue = pValue->pNext;
	//}


	// debug dump
	//dump_hash_table_2(pHashTable);
	//dump_list_of_ints(pListOfNumbers);

	// assign the working pointers
	*ppHashTable = pHashTable;
	*ppListOfNumbers = pListOfNumbers;

	return RC_OK;

}


list_t *compress(unsigned char *pUncompressed, unsigned int len) {

	unsigned char *pW = NULL;
	int lenW = 0;
	unsigned char *pC = NULL;
	const int lenC = 1;  // pC is the full buffer, but we always use just one char at a time.
	unsigned char *pWC = NULL;
	int lenWC = 0;

	list_t *pListIntsResult = NULL;  // This is the list of symbols which is the compressed result
	struct hash_table_str_key_int_value *pDict = NULL;
	node_t *pValue = NULL;
	unsigned int i;
	int iRC;
	bool bRC;
	struct key_value_pair_str_key_int_val *pKeyValuePair = NULL;
	int symbol;
	int dict_size = 256;


	// dump buffer
	//dump_buffer(pUncompressed);

	iRC = build_primatives_for_compression(&pDict, &pListIntsResult);
	if (RC_ERROR == iRC) {
		printf("Failed to create primitaves for compression.\n");
		return NULL;

	}

	//dump_hash_table_2(pHashTable);
	//dump_list_of_ints(pListIntsResult);





	//printf("index \t hex \t char\n");
	pC = pUncompressed;
	for (i = 0; i < len; i++) {

		//printf("index:%d \t %x \t %c \n", i, *pC, *pC);

		//
		// wc = w + c
		// 
		// 
		lenWC = lenW + lenC;
		pWC = (unsigned char *) realloc(pWC, lenWC);  

		// wc = w + c
		if (NULL != pW) {
			memcpy(pWC, pW, lenW);
		}
		// We can not strcat pC since its the pointer to the head of the buffer.  We only add one byte of the buffer to the end
		//strcat(pWC, pC);
		pWC[lenW] = *pC;  

		// if wc in dictionary
		bRC = isKeyInDict_2(pWC, lenWC,  pDict);
		if (bRC) {
			// w = wc
			free(pW);
			pW = NULL;
			pW = (unsigned char *) malloc(lenWC);
			memcpy(pW, pWC,lenWC);
			lenW = lenWC;
		} else {

			//
			// STEP 1: Append the symbol dictionary[w] to the result 
			// STEP 2: Add wc to the dictionary and increment size of dictionary
			//

			//
			// STEP 1: Obtain the symbol for byte sequence w and append to result list.
			//

			iRC = get_val_for_key_2(pDict, pW, lenW, &symbol);  // symbol = dict[w]
			if (RC_OK == iRC) {
				iRC = append2(pListIntsResult, &symbol, eMALLOC_INT); // results_list.append(symbol)
				if (iRC) {
					printf("Failed to append number to list.\n");
					return NULL;
				}
			} else {
				printf("an error occured when retrieving the last symbol for the key pW\n");
				return NULL;
			}


			//
			// STEP 2: Add wc to the dictionary and increment size of dictionary
			//

			// add wc to dictionary
			//  dictionary[wc] = dict_size
			iRC = add_key_val_pair_to_dict_2(pDict, pWC, lenWC, dict_size);
			if (RC_ERROR == iRC) {
				printf("insert has failed!\n");
				return NULL;
			}
			dict_size++;	// dict_size += 1


			// w = c
			free(pW);  
			pW = NULL;
			pW = (unsigned char *)malloc(lenC); // remember lenC is always just one byte
			pW[0] = *pC;  // w = c
			lenW = 1;

		}

		pC++;
	}

	// If there is a word left, add its corresponding symbol to the result list.
	if (NULL != pW) {
		iRC = get_val_for_key_2(pDict, pW, lenW, &symbol);
		if (RC_OK == iRC) {
			iRC = append2(pListIntsResult, &symbol, eMALLOC_INT);
			if (iRC) {
				printf("Failed to append number to list.\n");
				return NULL;
			}
		} else {
			printf("an error occured when retrieving the last symbol for the key pW\n");
			return NULL;
		}

	}

	//dump_hash_table_2(pDict);
	//dump_list_of_ints(pListIntsResult);

	// Free the pW and pWC
	free(pWC);
	free(pW);


	// Free the dictionary
	free_hash_table_2(pDict);


	return pListIntsResult;

}




//          if k in dictionary :
//              entry = dictionary[k]
//          elif k == dict_size :
//	            entry = w + w[0]
//          else :
//	            raise ValueError('Bad compressed k: %s' % k)
//	        result.write(entry)
//
//	        # Add w + entry[0] to the dictionary.
//	        dictionary[dict_size] = w + entry[0]
//	        dict_size += 1
//
//	        w = entry

// len is the lenght of the output buffer returned by the routine
unsigned char * decompress(list_t *pListSymbols, unsigned int *pLen) {

	struct hash_table_int_key_str_value *pDict = NULL;
	node_t *pNode = NULL;
	unsigned char *pchResult = NULL;
	unsigned char *pchEntry = NULL;
	unsigned char *pchEntryExisting = NULL;
	unsigned char *pchNewEntry = NULL;
	unsigned int *piValue = NULL;
	unsigned char *pW = NULL;
	int lenW = 0;
	unsigned char *pC = NULL;
	int lenC = 0;
	unsigned char *pWC = NULL;
	int lenWC = 0;
	bool bRC;
	int iRC;
	unsigned int iSymbol;  // rather than use the piValue, just keep it as a number.
	unsigned int dict_size = 256;
	int lenResult;
	int lenEntry;
	int lenNewEntry;

	if (NULL == pListSymbols) {
		return NULL;
	}


	// Build the dictionary
	// Build the initial dictionary with key:int value:byte sequence
	pDict = build_initial_dictionary_intKey_strValue();
	if (NULL == pDict) {
		return NULL;
	}

	if (NULL == pLen) {
		return NULL;
	}


	// dump_hash_table_1(pDict);


	//	    w = chr(compressed.pop(0))
	//	    result.write(w)

	// Aassume there is at least one symbol in the list. 
	// TODO: don't assume this!
	pW = (unsigned char *)malloc(1);  // (leak here )
	piValue = (unsigned int *) pop(pListSymbols, 0);  // pull the first value from the list identified as index 0.
	pW[0] = (unsigned char) *piValue;  // is an int value corresponding to an ascii value
	lenW = 1;
	free(piValue);  // we used append2() method to add these numbers so when we pop the values, we need to free it.
	piValue = NULL;

	pchResult = (unsigned char *) malloc(1);
	pchResult[0] = pW[0];
	lenResult = 1;

	// for k in compressed :

	pNode = pListSymbols->pFirst;
	while (NULL != pNode) {
		piValue = (unsigned int *) pNode->pData;
		iSymbol = *piValue;

		// if k in dictionary :  (in this code, k is symbol)
		bRC = isKeyInDict_1(iSymbol, pDict);
		if (bRC) {
			// symbol is in dictionary
			//  entry = dictionary[k]
			// In this case, entry is coming from the dictionary.  We do not free it, because
			// if we do we will in effect remove it from the dictionary!!!!
			// TLDR: pchEntryExisting is a pointer from the dictionary.
			//printf("symbol %d is a key in dict\n", iSymbol);
			iRC = get_val_for_key_1(pDict, iSymbol, &pchEntryExisting, &lenEntry);
			//printf("corresponding value is  %s \n", pchEntryExisting);
			// copy existing ref to one we can free.
			free(pchEntry);
			pchEntry = (unsigned char *) malloc(lenEntry); // one for entry[0] 
			pchEntry = (unsigned char *) memcpy(pchEntry, pchEntryExisting, lenEntry);



		} else if (iSymbol == dict_size) {
			// entry = w + w[0]
			free(pchEntry);
			pchEntry = NULL;
			// TLDR: pchEntry is a new allocated value.
			pchEntry = (unsigned char *) malloc(lenW + 1); // one for entry[0] 
			pchEntry = (unsigned char *) memcpy(pchEntry, pW, lenW);
			pchEntry[lenW] = pW[0];
			lenEntry = lenW + 1;

		} else {
			// symbol is not in dictionary
			printf("iSymbol %d is not a key in dict\n", iSymbol);
//			printf("bad compressed k: %s\n", k);
			return NULL;
		}


		//	result.write(entry)
		// lenEntry should be known from above lenEntry = (int) strlen(pchEntry);
		// lenResult should be known from above lenResult = (int) strlen(pchResult);
		pchResult = (unsigned char *) realloc(pchResult, lenResult + lenEntry);  // do not add one since we are not using null terminating zeros
		if (NULL == pchResult) {
			printf("failure performing realloc of pchResult in decompression routine.\n");
			return NULL;
		}
		memcpy( &pchResult[lenResult] , pchEntry, lenEntry);
		lenResult = lenResult + lenEntry;

		//
		//	# Add w + entry[0] to the dictionary.
		//	dictionary[dict_size] = w + entry[0]
		//	dict_size += 1
		//
		free(pchNewEntry);
		pchNewEntry = NULL;
		pchNewEntry = (unsigned char *) malloc(lenW + 1); // lenW + one for entry[0] 
		pchNewEntry = (unsigned char *) memcpy(pchNewEntry, pW, lenW);
		pchNewEntry[lenW] = pchEntry[0];
		lenNewEntry = lenW + 1;

		iRC = add_key_val_pair_to_dict_1(pDict, dict_size, pchNewEntry, lenNewEntry);
		if (RC_ERROR == iRC) {
			printf("failed to add symbol to dictionary\n");
			//			printf("bad compressed k: %s\n", k);
			return NULL;

		}
		dict_size++;

		//	w = entry
		free(pW);
		pW = NULL;
		lenW = lenEntry;
		pW = (unsigned char *) malloc(lenW);
		memcpy(pW, pchEntry, lenEntry);

		// Iterate the list of symbols
		pNode = pNode->pNext;
	}


    // Free the dictionary
	free_hash_table_1(pDict);


	// free buffers
	free(pW);
	free(pchNewEntry);
	free(pchEntry);  // We can now free pchEntry since its either allocated new or allocated with copy from dictionary

	*pLen = lenResult;

	return pchResult;

}
