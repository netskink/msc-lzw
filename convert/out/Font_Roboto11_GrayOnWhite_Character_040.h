
// created yyyy-mm-dd 

#ifndef Font_Roboto11_GrayOnWhite_Character_040_H
#define Font_Roboto11_GrayOnWhite_Character_040_H

#include "assets.h" 

extern const short int Font_Roboto11_GrayOnWhite_Character_040_x;
extern const short int Font_Roboto11_GrayOnWhite_Character_040_y;
extern const unsigned short Font_Roboto11_GrayOnWhite_Character_040[];
extern const image_t imageFont_Roboto11_GrayOnWhite_Character_040;


#endif


