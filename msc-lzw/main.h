#pragma once
// this is the header file
const int RC_OK = 0;
const int RC_ERROR = 1;


typedef enum {
	NO_COMPRESSION  = 0,		// Read a bmp file, extract the pixel array write the pixels w/o compression to a .c and .h file.
	COMPRESS_FILE   = 1,		// Compress a file
	COMPRESS_BITMAP = 2,		// Read a file, extract the pixel array and compress the pixels to a .c and .h file.
} compress_mode_t;


// Uncomment below to test the list
//#define TEST_LIST
 

// Uncomment below to test the dictionary
//#define TEST_DICTIONARY


// Uncomment below to test the bitmap code
//#define TEST_BITMAP

// Uncomment below to test the utils code
//#define TEST_UTILS


