
// created yyyy-mm-dd 

#ifndef Font_Roboto11_GrayOnWhite_Character_058_H
#define Font_Roboto11_GrayOnWhite_Character_058_H

#include "assets.h" 

extern const short int Font_Roboto11_GrayOnWhite_Character_058_x;
extern const short int Font_Roboto11_GrayOnWhite_Character_058_y;
extern const unsigned short Font_Roboto11_GrayOnWhite_Character_058[];
extern const image_t imageFont_Roboto11_GrayOnWhite_Character_058;


#endif


