
// created yyyy-mm-dd 

#include "assets.h" 

const short int Font_Roboto11_Bold_GrayOnWhite_Character_064_x = 15;
const short int Font_Roboto11_Bold_GrayOnWhite_Character_064_y = 16;

const unsigned short Font_Roboto11_Bold_GrayOnWhite_Character_064[] = {
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0x0abc, 0x2a32, 0x2a32, 0x2a32, 0x2a32, 0x2a32, 0x1834, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x15ff, 0x2a52, 
0x2a32, 0x1834, 0xffff, 0xffff, 0xffff, 0x0abc, 0x2a32, 0x2d32, 
0x1f9f, 0xffff, 0xffff, 0xffff, 0xf8ff, 0x2a7a, 0x2a32, 0x1c55, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0ddd, 0x2a32, 0x3533, 
0xffdf, 0xffff, 0xffff, 0x0abc, 0x2a32, 0x1834, 0xffff, 0x0abc, 
0x2a32, 0x2a32, 0x2a32, 0x3533, 0xfcdf, 0x2a9b, 0x2a32, 0x1f7e, 
0xffff, 0xf8ff, 0x2a7a, 0x2a32, 0x1f7e, 0x0ddd, 0x2a32, 0x3132, 
0xf8bf, 0x2a7a, 0x2a32, 0x1f7e, 0x0ddd, 0x2a32, 0x1c55, 0xffff, 
0x15ff, 0x2a52, 0x3132, 0xfcbf, 0x2a9b, 0x2a32, 0x1c55, 0x15ff, 
0x2a52, 0x2d32, 0x1f9f, 0x0ddd, 0x2a32, 0x1834, 0xffff, 0x11fe, 
0x2a32, 0x3132, 0x15bf, 0x2a52, 0x2a32, 0x1f7e, 0x11fe, 0x2a32, 
0x3132, 0xffbf, 0x0ddd, 0x2a32, 0x1834, 0xffff, 0x11fe, 0x2a32, 
0x3132, 0x15bf, 0x2a52, 0x2a32, 0x1f7e, 0x11fe, 0x2a32, 0x3132, 
0xffbf, 0x0abc, 0x2a32, 0x1c55, 0xffff, 0x15ff, 0x2a52, 0x3132, 
0x15bf, 0x2a52, 0x2a32, 0x1c55, 0x0abc, 0x2a32, 0x3132, 0xf8bf, 
0x2a7a, 0x3132, 0xffbf, 0xffff, 0xf8ff, 0x2a7a, 0x2a32, 0x1f7e, 
0x0ddd, 0x2a32, 0x2a32, 0x2a32, 0x2a32, 0x2a32, 0x2a32, 0x2d32, 
0x1f9f, 0xffff, 0xffff, 0xffff, 0x0ddd, 0x2a32, 0x3132, 0xffbf, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0x0abc, 0x2a32, 0x2d32, 0x1f9f, 
0xffff, 0xffff, 0x1cff, 0x1cdf, 0xffdf, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0x15ff, 0x2a52, 0x2a32, 0x2a32, 
0x2a32, 0x2a32, 0x2d32, 0x1f9f, 0xffff, 0xffff, 0xffff, 0xffff, 
};


const image_t imageFont_Roboto11_Bold_GrayOnWhite_Character_064 = {
	Font_Roboto11_Bold_GrayOnWhite_Character_064_x,
	Font_Roboto11_Bold_GrayOnWhite_Character_064_y,
	Font_Roboto11_Bold_GrayOnWhite_Character_064
};


