// msc-lzw.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"



#include <Windows.h>

#include <string.h>
#include "main.h"
#include "dictionary.h"
#include "list.h"
#include "lzw.h"
#include "file_io.h"
#include "ns_process.h"
#include "bitmap.h"
#include "getopt.h"  // my version of getopt lifted from a port to windows 
#include "utils.h"
#include "version.h"

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>	



// test inputs
// test_4_8x4_win.bmp  made with windows10 version of paint.  The outcome is a swapped version suitable for dma.  ie. no need to swap prior to dma.
// Font_Roboto11_WhiteOnBlue_Character_088.bmp
// test_5_8x4_win7.bmp  same as test4 but created on windows7
// Font_Roboto11_WhiteOnBlue_Character_049.bmp

void print_usage(void) {
	printf(" \n");
	printf("Usage: msc-lzw [-m <RG_GB|GB_RG>] [-n] [-h] [-d <output_dir>] <file_name> \n");
	printf("By default it compresses a file as is. \n");
	printf("\t The output file name is the 'input-file-name-root.lzw'.\n");
	printf("If the -m mode option is specifed, it assumes the input file is a bitmap.\n");
	printf("\t It attempts to write a 16-bit RGB .c and .h file based upon the given option.  See -n option.\n");
	printf("\t RG_GB mode is the mode used by the embedded code for DMA with no swaps.\n");
	printf("If the -n option is specified, it assumes the input file is a bitmap.\n");
	printf("\t Furthermore, the -n option means that no compression is used when the .c and .h file are written.\n");
	printf("If the -h option is specifed, it displays this help.\n");
	printf("If the -V option is specifed, it displays the settings used.\n");
	printf("If the -d option is specifed, it writes output to the provided directory.\n");
}

void set_mode(rgb16_byte_order_t *peByteOrder, char *pchModeSetting) {
	int iRC;
	iRC = strcmp("RG_GB", pchModeSetting);
	if (0 == iRC) {
		*peByteOrder = RG_GB;
		return;
	}
	iRC = strcmp("GB_RG", pchModeSetting);
	if (0 == iRC) {
		*peByteOrder = GB_RG;
		return;
	}

	printf("*** Unknown byte order. Exit Failure.\n");
	print_usage();
	exit(EXIT_FAILURE);
}


void set_output_dir_name(char *pOutputDirName, char *pchDirSetting) {
	strcpy(pOutputDirName, pchDirSetting);
}

void set_compression(compress_mode_t *peCompressMode) {
	*peCompressMode = NO_COMPRESSION;
}

void dump_settings(char *pchInputFileName, char *pchOutDirName, char *pchOutputFileName, char *pchTag, rgb16_byte_order_t eByteOrder, compress_mode_t eCompressMode) {

	printf("Settings:\n");

	printf("\tInput File Name = %s\n", pchInputFileName);
	printf("\tOutput Dir Name = %s\n", pchOutDirName);
	printf("\tFile Name = %s\n", pchOutputFileName);
	printf("\t\tOnly valid for file compression.  Meaningless for .c and .h files. \n");
	printf("\ttag = %s\n", pchTag);
	printf("\t\tUsed to determine the output file name and variable names in .c and .h files.\n");
	if (RG_GB == eByteOrder) {
		printf("\tByte mode = RG_GB. DMA ready without swapping.\n");
	} else if (GB_RG == eByteOrder) {
		printf("\tByte mode = GB_RG. Will require swapping when doing DMA.\n");

	}
	if (NO_COMPRESSION == eCompressMode) {
		printf("\tSet to read a bitmap and then write pixel array as .c and .h file without compression.\n");
	} else if (COMPRESS_FILE == eCompressMode) {
		printf("\tSet to perform whole file compression using lzw.\n");

	} else if (COMPRESS_BITMAP == eCompressMode) {
		printf("\tSet to read a bitmap and then write pixel array as .c and .h file with LZW compression.\n");
	}

}


#if !defined(TEST_LIST) && !defined(TEST_DICTIONARY) && !defined(TEST_BITMAP) && !defined(TEST_UTILS)
int main(int argc, char *argv[]) {

	int iRC;
	int nRows, nCols;
	unsigned char *pBufferInput = NULL;
	unsigned char *pBufferDecompressed = NULL;
	list_t *pCompressedListSymbol = NULL;
	unsigned int len;

	int option = 0;
	int optidx = 0;
	rgb16_byte_order_t eByteOrder = RG_GB; // by default the order is RG_GB for bytes.  This is used for DMA routines.
	compress_mode_t eCompressMode = COMPRESS_FILE;  // by default compress a file.
	bool do_dump_settings = false;

	char pInputFileName[100];
	char pOutputFileName[80] = "input00.lzw";
	char pOutputDirName[80] = "out2";
	char pCompressedInputFileName[80] = "input00.lzw";
	char pTag[80] = "test00";


	//
	// Time to determine the filename and program options from the command args
	//

	// There must be at least one argument provided.
	if (argc < 2) {
		printf("Must specify at least one argument which by default is the input filename.\n");
		print_usage();
		exit(EXIT_FAILURE);
	}



	//
	// Determine the program options
	//
	for (;;) {

		option = getopt(argc, argv, "Vhvnm:d:");
		if (-1 == option)
			break;

		switch (option) {
		case 'h': print_usage(); optidx++; break;
		case 'v': print_version(); optidx++; break;
		case 'm': set_mode(&eByteOrder, optarg); optidx++; optidx++; break;
		case 'd': set_output_dir_name(pOutputDirName, optarg); optidx++; optidx++; break;
		case 'n': set_compression(&eCompressMode); optidx++; break;
		case 'V': do_dump_settings=true; optidx++; break;
		}



	}

	// So, after processing options, there should be two args left.  The program name in pos[0] and the 
	// input file name in the last position.
	if ( 2 != (argc - optidx) ) {
		printf("some unknown option given. Exit failure.\n");
		print_usage();
		exit(EXIT_FAILURE);
	}

	// to stop in debugger
	//int bloop = 1;
	//while (bloop);

	//
	// parse remaing cmdline options to determine the filename
	// and the tag.
	// 
	// assuming the last option is the inputfilename
	// yes, you could overflow this buffer.
	strcpy(pInputFileName,argv[argc-1]);
	iRC =  determine_rootname(pInputFileName, pTag);
	if (RC_ERROR == iRC) {
		printf("An error occured while parsing the input file name in order to determine the rootname/tag.\n");
		return RC_ERROR;
	}

	// set the output filename based upon the tag
	strcpy(pOutputFileName, pTag);
	strcat(pOutputFileName,".lzw");





	// dump the settings if requested.
	if (do_dump_settings) {
		dump_settings(pInputFileName, pOutputDirName, pOutputFileName, pTag, eByteOrder, eCompressMode);
	}


	


	if (COMPRESS_FILE == eCompressMode) {
		// This is for compressing an entire file.
		len = read_file_into_buffer(pInputFileName, &pBufferInput);
		if (len < 0) {
			printf("An error occured while reading the input file\n");
			return RC_ERROR;
		}

	} else {
		// read the bitmap file and return the image array. Also determines the number of rows
		// and number of columns.
		iRC = FillAndAllocate(pInputFileName, pBufferInput, len, eByteOrder, nRows, nCols);
		if (RC_ERROR == iRC) {
			printf("An error occured while parsing the input bitmap file.\n");
			return RC_ERROR;
		}
	}




	if (COMPRESS_FILE == eCompressMode || COMPRESS_BITMAP == eCompressMode) {
		// compress buffer into a list of symbols
		pCompressedListSymbol = compress(pBufferInput, len);
		if (NULL == pCompressedListSymbol) {
			printf("An error occured while compressing the input buffer\n");
			return RC_ERROR;
		}
		dump_list_of_ints(pCompressedListSymbol);

		// convert the list into an array
		unsigned short int *pArray = NULL;
		int iArrayLen;
		iRC = convert_list_of_ints_to_array(pCompressedListSymbol, &pArray, &iArrayLen);
		if (RC_ERROR == iRC) {
			printf("An error occured while converting list to array.\n");
			return RC_ERROR;

		}

		list_t *pList = NULL;
		iRC = convert_array_of_shorts_to_list_of_ints(pArray, iArrayLen, &pList);
		if (RC_ERROR == iRC) {
			printf("An error occured while converting array to list.\n");
			return RC_ERROR;

		}

		dump_list_of_shorts(pList);

		// Write the .c file as an array of compressed ints
		iRC = write_compressed_c_array(pCompressedListSymbol, pTag);
		if (RC_ERROR == iRC) {
			printf("An error occured while writing the .c file.\n");
			return RC_ERROR;
		}

		// decompress buffer
		pBufferDecompressed = decompress(pCompressedListSymbol, &len);
		if (NULL == pBufferDecompressed) {
			printf("An error occured while reading the input file\n");
			return RC_ERROR;
		}
		//// We no longer use the buffer as string.  Get rid of the strlen() call.
		iRC = write_buffer_to_file(pOutputFileName, pBufferDecompressed, (unsigned int) len); // TODO: fix typecast 
		if (RC_ERROR == iRC) {
			printf("An error occured while writing the decompressed output file\n");
			return RC_ERROR;
		}

		// write the compressed buffer
		// TODO: write the compressed buffer


		//// No longer need the compressed buffer, free it.
		free(pBufferDecompressed);

		// No longer need the compressed list
		free_list(pCompressedListSymbol);



	} else {

		//
		// No compression
		//

		iRC = write_uncompressed_c_array(pOutputDirName, pBufferInput, (unsigned int ) len, pTag, nRows, nCols);
		if (RC_ERROR == iRC) {
			printf("An error occured while writing the .c file.\n");
			return RC_ERROR;
		}

	}








	// No longer need the input buffer, free it
	free(pBufferInput);
	pBufferInput = NULL;




	// Verify the compression and decompression routines work
	// by comparing the checksums
	if (COMPRESS_FILE == eCompressMode) {

		iRC = didCompressionDecompressionWork(pInputFileName, pOutputFileName);
		if (RC_ERROR == iRC) {
			printf("The input file does not match the compressed and then decompressed output file\n");
			return RC_ERROR;
		} else {
			printf("Excellent.  Input and outpufiles match.");
		}

	} else {
		printf("Skipping compare of bitmaps since we are writing .c and .h files.\n");
	}


	_CrtDumpMemoryLeaks();
	return EXIT_SUCCESS;
}

#endif

