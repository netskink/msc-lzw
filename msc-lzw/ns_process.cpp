
#include "stdafx.h"

#include <Windows.h>

#include <string.h>
#include "main.h"
#include "dictionary.h"
#include "list.h"
#include "lzw.h"
#include "file_io.h"
#include "ns_process.h"
#include <strsafe.h>

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>	





#define BUFSIZE 4096 

HANDLE g_hChildStd_IN_Rd = NULL;
HANDLE g_hChildStd_IN_Wr = NULL;
HANDLE g_hChildStd_OUT_Rd = NULL;
HANDLE g_hChildStd_OUT_Wr = NULL;




// Create a child process that uses the previously created pipes for STDIN and STDOUT.
void CreateChildProcess(const char *pchFileName1, char *pchFileName2) {

	// Used to build command line.  Need to be free upon completion of child process
	// a tchar is either a regular char or a 16-bit unicode char depending upon platform.
	// These were used earlier but they can't work well with free or delete.  Finally
	// used the windows api's for LocalAlloc and LocalFree to get it to work.
	// tchars bring back bad memories from working in tools group on the windows userspace stack for the
	// mpx debug driver. 
	//LPTSTR szCmdLine = _tcsdup(TEXT("C:\\Program Files\\Git\\usr\\bin\\md5sum.exe"));
	//LPTSTR szCmdArgs = _tcsdup(TEXT("md5sum.exe"));  // the first parm is the program name again, for use with runtime provided strings
													 //	LPTSTR szCmdargs = _tcsdup(TEXT("md5sum.exe BlueBattery.bmp output.txt"));  // with hard coded strings


	// Used to convert from char * to LPSTR so we can use the createProcess() routine
	const size_t WCHAR_BUF_LEN = 500;
	wchar_t wszDest[WCHAR_BUF_LEN];
	LPTSTR szCmdLine = NULL;
	LPTSTR szCmdArgs = NULL;


	// alloc space for initial strings
	szCmdLine = (LPTSTR) LocalAlloc(LPTR, WCHAR_BUF_LEN * sizeof(TCHAR));
	if (NULL == szCmdLine) {
		ErrorExit(TEXT("LocalAlloc 1"));
	}

	szCmdArgs = (LPTSTR) LocalAlloc(LPTR, WCHAR_BUF_LEN * sizeof(TCHAR));
	if (NULL == szCmdArgs) {
		ErrorExit(TEXT("LocalAlloc 2"));
	}

	// Build the cmdLine
	MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, "C:\\Program Files\\Git\\usr\\bin\\md5sum.exe", -1, wszDest, WCHAR_BUF_LEN);  // create a fully qualified pathname string
	wcscat(szCmdLine, wszDest); // copy to 

	// Build the cmdArgs
	MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, "md5sum.exe", -1, wszDest, WCHAR_BUF_LEN);  // create a "md5sum" string
	wcscat(szCmdArgs, wszDest); // concat a space


	MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, " ", -1, wszDest, WCHAR_BUF_LEN);  // create a " " string
	wcscat(szCmdArgs, wszDest); // concat a space
	MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, pchFileName1, -1, wszDest, WCHAR_BUF_LEN);  // create a " " string
	wcscat(szCmdArgs, wszDest);  // concat filename 1
	MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, " ", -1, wszDest, WCHAR_BUF_LEN);  // create a " " string
	wcscat(szCmdArgs, wszDest); // concat a space
	MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, pchFileName2, -1, wszDest, WCHAR_BUF_LEN);  // create a " " string
	wcscat(szCmdArgs, wszDest); // concat filename 2

//	LPVOID lpEnvironment = _tcsdup(TEXT("PATH=C:\\Program Files\\git\\cmd"));

	// TODO keep around for reference
	// Copy environment strings into an environment block. 
	//TCHAR chNewEnv[BUFSIZE];
	//LPTSTR lpszCurrentVariable;
	//lpszCurrentVariable = (LPTSTR) chNewEnv;
	//if (FAILED(StringCchCopy(lpszCurrentVariable, BUFSIZE, TEXT("PATH=%PATH;C:\\Program Files\\git\\usr\\bin")))) {
	//	printf("String copy failed\n");
	//	return;
	//}

	//// Terminate the block with a NULL byte. 

	//lpszCurrentVariable += lstrlen(lpszCurrentVariable) + 1;
	//*lpszCurrentVariable = (TCHAR) 0;



	PROCESS_INFORMATION piProcInfo;
	STARTUPINFO siStartInfo;
	BOOL bSuccess = FALSE;

	// Set up members of the PROCESS_INFORMATION structure. 

	ZeroMemory(&piProcInfo, sizeof(PROCESS_INFORMATION));

	// Set up members of the STARTUPINFO structure. 
	// This structure specifies the STDIN and STDOUT handles for redirection.

	ZeroMemory(&siStartInfo, sizeof(STARTUPINFO));
	siStartInfo.cb = sizeof(STARTUPINFO);

	// uncomment the define to see stdout and stderr on console.
	// This will prevent the pipes from working though.
//#define DEBUG_CHILD_PROCESS
#ifdef DEBUG_CHILD_PROCESS
	// TODO: currently this will hang the read pipe code, because
	// its set to read the result.  This will allow you to read the
	// output via stdout on terminal though.
	siStartInfo.hStdError = NULL;
	siStartInfo.hStdOutput = NULL;
	siStartInfo.hStdInput = NULL;
#else
	siStartInfo.hStdError = g_hChildStd_OUT_Wr;
	siStartInfo.hStdOutput = g_hChildStd_OUT_Wr;
	siStartInfo.hStdInput = g_hChildStd_IN_Rd;
	siStartInfo.dwFlags |= STARTF_USESTDHANDLES;
#endif

	// Create the child process. 
//	(LPVOID) chNewEnv,          // modified environment 

	bSuccess = CreateProcess(szCmdLine,     // cmd line
		                     szCmdArgs,     // command line ags 
							 NULL,          // process security attributes 
							 NULL,          // primary thread security attributes 
							 TRUE,          // handles are inherited 
							 0,             // creation flags 
							 NULL,          // use parent's environment 
							 NULL,          // use parent's current directory 
							 &siStartInfo,  // STARTUPINFO pointer 
							 &piProcInfo);  // receives PROCESS_INFORMATION 

					   // If an error occurs, exit the application. 
	if (!bSuccess) {

		ErrorExit(TEXT("CreateProcess"));

	}  else {


		// Wait for child process to complete
		WaitForSingleObject(piProcInfo.hProcess, INFINITE);



		// Close handles to the child process and its primary thread.
		// Some applications might keep these handles to monitor the status
		// of the child process, for example. 

		CloseHandle(piProcInfo.hProcess);
		CloseHandle(piProcInfo.hThread);
	}



	// Free the buffers used to start the child
	LocalFree(szCmdLine);
	LocalFree(szCmdArgs);


}



// Read output from the child process's pipe for STDOUT
// and write to the parent process's pipe for STDOUT. 
// Stop when there is no more data. 
int ReadFromPipe(unsigned char *pchBuffer, unsigned long int bufLen) {

	DWORD bytesRead;
	BOOL bRC = FALSE;
	HANDLE hParentStdOut = GetStdHandle(STD_OUTPUT_HANDLE);

	// chBuf will contain the entire output.  It will look like this:
	//fb5df6f0265324e815c95fa7a3fc5d9a *BlueBattery.bmp<0x0A>
	//fb5df6f0265324e815c95fa7a3fc5d9a *output.txt<0x0A>
	bRC = ReadFile(g_hChildStd_OUT_Rd, pchBuffer, bufLen, &bytesRead, NULL);
		
	if (FALSE == bRC) {
		printf("Failed to read pipe from child process.\n");
		return RC_ERROR;
	}

	if (0 == bytesRead) {
		printf("Failed to read pipe from child process.\n");
		return RC_ERROR;
	}

	return RC_OK;
}

int parseBuffer(unsigned char *pchBuffer, unsigned long int bufLen) {

	int lineOneEnd = 0;
	int lineTwoEnd = 0;
	unsigned int i,j;
	//char pchHash1[32];
	//char pchHash1[32];

	// The buffer will look like this:
	//fb5df6f0265324e815c95fa7a3fc5d9a *BlueBattery.bmp<0x0A>
	//fb5df6f0265324e815c95fa7a3fc5d9a *output.txt<0x0A>

	// Find the two spaces which identify the end of the hash
	for (i = 0; i < bufLen; i++) {
		if (0x20 == pchBuffer[i]) {
			if (0 == lineOneEnd && 0 == lineTwoEnd) {
				lineOneEnd = i;
			} else if (0 != lineOneEnd && 0 == lineTwoEnd) {
				lineTwoEnd = i;
			} else {
				// More than two lines or some other error
				return RC_ERROR;
			}
		}
	}

	// We should find two spaces, verify the values make sense
	if (0 == lineOneEnd || 0 == lineTwoEnd) {
		return RC_ERROR;
	}
	if (lineTwoEnd < lineOneEnd) {
		return RC_ERROR;
	}


	// compare the two hashes
	i = 0; // index for hash one
	j = lineTwoEnd - 32;
	while (i < 32) {
		if (pchBuffer[i] == pchBuffer[j]) {
			i++; j++;
		} else {
			return RC_ERROR;
		}
	}

	return RC_OK;
}

// Format a readable error message, display a message box, 
// and exit from the application.
void ErrorExit(PTSTR lpszFunction) {

	LPVOID lpMsgBuf;
	LPVOID lpDisplayBuf;
	DWORD dw = GetLastError();

	FormatMessage( FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
					NULL,
					dw,
					MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
					(LPTSTR) &lpMsgBuf,
					0, 
					NULL);

	lpDisplayBuf = (LPVOID) LocalAlloc(LMEM_ZEROINIT,( lstrlen((LPCTSTR) lpMsgBuf) + lstrlen((LPCTSTR) lpszFunction) + 40 ) * sizeof(TCHAR));
	
	StringCchPrintf((LPTSTR) lpDisplayBuf,
		            LocalSize(lpDisplayBuf) / sizeof(TCHAR),
		            TEXT("%s failed with error %d: %s"),
		            lpszFunction, 
		            dw, 
		            lpMsgBuf);

	MessageBox(NULL, (LPCTSTR) lpDisplayBuf, TEXT("Error"), MB_OK);

	LocalFree(lpMsgBuf);
	LocalFree(lpDisplayBuf);
	ExitProcess(1);

}


int didCompressionDecompressionWork(const char *pchFileName1, char *pchFileName2) {

	SECURITY_ATTRIBUTES saAttr;
	BOOL bRC;
	int iRC;
	CHAR chBuf[BUFSIZE];


	// Set the bInheritHandle flag so pipe handles are inherited. 
	saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
	saAttr.bInheritHandle = TRUE;
	saAttr.lpSecurityDescriptor = NULL;

	// Create a pipe for the child process's STDOUT. 
	bRC = CreatePipe(&g_hChildStd_OUT_Rd, &g_hChildStd_OUT_Wr, &saAttr, 0);
	if (FALSE == bRC) {
		ErrorExit(TEXT("StdoutRd CreatePipe"));
	}

	// Ensure the read handle to the pipe for STDOUT is not inherited.
	bRC = SetHandleInformation(g_hChildStd_OUT_Rd, HANDLE_FLAG_INHERIT, 0);
	if (FALSE == bRC) {
		ErrorExit(TEXT("Stdout SetHandleInformation"));
	}

	// Create a pipe for the child process's STDIN. 
	bRC = CreatePipe(&g_hChildStd_IN_Rd, &g_hChildStd_IN_Wr, &saAttr, 0);
	if (FALSE == bRC) {
		ErrorExit(TEXT("Stdin CreatePipe"));
	}

	// Ensure the write handle to the pipe for STDIN is not inherited. 
	bRC = SetHandleInformation(g_hChildStd_IN_Wr, HANDLE_FLAG_INHERIT, 0);
	if (FALSE == bRC) {
		ErrorExit(TEXT("Stdin SetHandleInformation"));
	}

	// Create the child process. 
	CreateChildProcess(pchFileName1, pchFileName2); 



	// Read from pipe that is the standard output for child process. 
	iRC = ReadFromPipe((unsigned char *) chBuf, BUFSIZE);
	if (RC_ERROR == iRC) {
		ErrorExit(TEXT("ReadFromPipe() failed."));
	}

	iRC = parseBuffer((unsigned char *) chBuf, BUFSIZE);
	if (RC_ERROR == iRC) {
		return RC_ERROR;;
	}


	// The remaining open handles are cleaned up when this process terminates. 
	// To avoid resource leaks in a larger application, close handles explicitly. 

	// Free the buffers used to start the child
//	free(szCmdLine);
//	free(szCmdArgs);

	return RC_OK;
}



