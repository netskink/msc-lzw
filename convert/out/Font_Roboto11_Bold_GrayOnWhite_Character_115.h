
// created yyyy-mm-dd 

#ifndef Font_Roboto11_Bold_GrayOnWhite_Character_115_H
#define Font_Roboto11_Bold_GrayOnWhite_Character_115_H

#include "assets.h" 

extern const short int Font_Roboto11_Bold_GrayOnWhite_Character_115_x;
extern const short int Font_Roboto11_Bold_GrayOnWhite_Character_115_y;
extern const unsigned short Font_Roboto11_Bold_GrayOnWhite_Character_115[];
extern const image_t imageFont_Roboto11_Bold_GrayOnWhite_Character_115;


#endif


