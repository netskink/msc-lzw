
// created yyyy-mm-dd 

#ifndef Font_Roboto11_GrayOnWhite_Character_073_H
#define Font_Roboto11_GrayOnWhite_Character_073_H

#include "assets.h" 

extern const short int Font_Roboto11_GrayOnWhite_Character_073_x;
extern const short int Font_Roboto11_GrayOnWhite_Character_073_y;
extern const unsigned short Font_Roboto11_GrayOnWhite_Character_073[];
extern const image_t imageFont_Roboto11_GrayOnWhite_Character_073;


#endif


