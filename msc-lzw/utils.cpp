

#include <stdio.h>

#include <string.h>
#include "main.h"
#include "utils.h"

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>	




// assumes it is a zero terminating buffer
void dump_string_buffer(char *pBuffer) {

	int len;

	len = (int)strlen(pBuffer);

	printf("index \t hex \t char\n");
	for (int i = 0; i < len; i++) {
		printf("index:%d \t %x \t %c \n", i, pBuffer[i], pBuffer[i]);
	}

}

// does not assume its a null terminated char string buffer
void dump_buffer(char *pBuffer, int len) {


	printf("index \t hex \t char\n");
	for (int i = 0; i < len; i++) {
		printf("index:%d \t %x \t %c \n", i, pBuffer[i], pBuffer[i]);
	}

}



void byte_swap16(unsigned short int *pVal16) {

//#define method_one 1
// #define method_two 1
#define method_three 1
#ifdef method_one
	unsigned char *pByte;

	pByte = (unsigned char *) pVal16;
	*pVal16 = (pByte[0] << 8) | pByte[1];
#endif

#ifdef method_two
	unsigned char *pByte0;
	unsigned char *pByte1;

	pByte0 = (unsigned char *) pVal16;
	pByte1 = pByte0 + 1;
	*pByte0 = *pByte0 ^ *pByte1;
	*pByte1 = *pByte0 ^ *pByte1;
	*pByte0 = *pByte0 ^ *pByte1;
#endif

#ifdef method_three
	unsigned char *pByte;

	pByte = (unsigned char *) pVal16;
	pByte[0] = pByte[0] ^ pByte[1];
	pByte[1] = pByte[0] ^ pByte[1];
	pByte[0] = pByte[0] ^ pByte[1];
#endif


}



void byte_swap32(unsigned int *pVal32) {

#ifdef method_one
	unsigned char *pByte;

	// 0x1234 5678 --> 0x7856 3412  
	pByte = (unsigned char *) pVal32;
	*pVal32 = ( pByte[0] << 24 ) | (pByte[1] << 16) | (pByte[2] << 8) | ( pByte[3] );
#endif

#if defined(method_two) || defined (method_three)
	unsigned char *pByte;

	pByte = (unsigned char *) pVal32;
	// move lsb to msb
	pByte[0] = pByte[0] ^ pByte[3];
	pByte[3] = pByte[0] ^ pByte[3];
	pByte[0] = pByte[0] ^ pByte[3];
	// move lsb to msb
	pByte[1] = pByte[1] ^ pByte[2];
	pByte[2] = pByte[1] ^ pByte[2];
	pByte[1] = pByte[1] ^ pByte[2];
#endif


}


//
// assumes a null terminated string.
// assumes the output buffer for the root filename has been allocated
// 
int determine_rootname(char *pchFilename, char *pchRootname) {


	if (NULL == pchFilename) {
		return RC_ERROR;
	}
	if (NULL == pchRootname) {
		return RC_ERROR;
	}


	// find the dir seperator
	char *pSep;
	pSep = strchr(pchFilename, '/');
	if (NULL == pSep) {
		// no sep for dir and file name

	} else {
		pchFilename = pSep + 1;
	}


	pchRootname = strcpy(pchRootname, pchFilename);
	if (NULL == pchRootname) {
		return RC_ERROR;
	}

	// find the '.'
	char *pPeriod;
	pPeriod = strchr(pchRootname, '.');
	if (NULL == pPeriod) {
		return RC_ERROR;
	}

	*pPeriod = 0;



	return RC_OK;
}


#ifdef TEST_UTILS
int main(int argc, char *argv[]) {

	unsigned short int u16Val = 0x1234;
	byte_swap16(&u16Val);

	if (0x3412 != u16Val) {
		return EXIT_FAILURE;
	}


	unsigned int u32Val = 0x12345678;
	byte_swap32(&u32Val);

	if (0x78563412 != u32Val) {
		return EXIT_FAILURE;
	}


	_CrtDumpMemoryLeaks();
	return EXIT_SUCCESS;

}

#endif
