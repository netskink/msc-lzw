
// created yyyy-mm-dd 

#include "assets.h" 

const short int Font_Roboto11_WhiteOnBlue_Character_045_x = 6;
const short int Font_Roboto11_WhiteOnBlue_Character_045_y = 16;

const unsigned short Font_Roboto11_WhiteOnBlue_Character_045[] = {
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x9f56, 0xffff, 0xffff, 0xffff, 0x1dbe, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
};


const image_t imageFont_Roboto11_WhiteOnBlue_Character_045 = {
	Font_Roboto11_WhiteOnBlue_Character_045_x,
	Font_Roboto11_WhiteOnBlue_Character_045_y,
	Font_Roboto11_WhiteOnBlue_Character_045
};


