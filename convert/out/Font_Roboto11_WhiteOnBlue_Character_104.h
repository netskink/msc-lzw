
// created yyyy-mm-dd 

#ifndef Font_Roboto11_WhiteOnBlue_Character_104_H
#define Font_Roboto11_WhiteOnBlue_Character_104_H

#include "assets.h" 

extern const short int Font_Roboto11_WhiteOnBlue_Character_104_x;
extern const short int Font_Roboto11_WhiteOnBlue_Character_104_y;
extern const unsigned short Font_Roboto11_WhiteOnBlue_Character_104[];
extern const image_t imageFont_Roboto11_WhiteOnBlue_Character_104;


#endif


