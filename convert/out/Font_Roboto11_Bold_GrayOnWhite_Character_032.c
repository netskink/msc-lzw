
// created yyyy-mm-dd 

#include "assets.h" 

const short int Font_Roboto11_Bold_GrayOnWhite_Character_032_x = 4;
const short int Font_Roboto11_Bold_GrayOnWhite_Character_032_y = 16;

const unsigned short Font_Roboto11_Bold_GrayOnWhite_Character_032[] = {
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
};


const image_t imageFont_Roboto11_Bold_GrayOnWhite_Character_032 = {
	Font_Roboto11_Bold_GrayOnWhite_Character_032_x,
	Font_Roboto11_Bold_GrayOnWhite_Character_032_y,
	Font_Roboto11_Bold_GrayOnWhite_Character_032
};


