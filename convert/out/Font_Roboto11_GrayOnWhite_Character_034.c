
// created yyyy-mm-dd 

#include "assets.h" 

const short int Font_Roboto11_GrayOnWhite_Character_034_x = 6;
const short int Font_Roboto11_GrayOnWhite_Character_034_y = 16;

const unsigned short Font_Roboto11_GrayOnWhite_Character_034[] = {
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0x0ddd, 0x3132, 0x15bf, 0x2a52, 
0x1f7e, 0xffff, 0x0ddd, 0x3132, 0x15bf, 0x2a52, 0x1f7e, 0xffff, 
0x0ddd, 0x3533, 0x15df, 0x2d52, 0x1f9f, 0xffff, 0x0ddd, 0x1c55, 
0x15ff, 0x3553, 0xffdf, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
};


const image_t imageFont_Roboto11_GrayOnWhite_Character_034 = {
	Font_Roboto11_GrayOnWhite_Character_034_x,
	Font_Roboto11_GrayOnWhite_Character_034_y,
	Font_Roboto11_GrayOnWhite_Character_034
};


