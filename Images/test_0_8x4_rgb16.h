#ifndef test_0_8x4_rgb16_H
#define test_0_8x4_rgb16_H


#include "assets.h"

extern const short int test_0_8x4_rgb16_x;
extern const short int test_0_8x4_rgb16_y;
extern const unsigned short test_0_8x4_rgb16[];
extern const image_t imagetest_0_8x4_rgb16;


#endif

