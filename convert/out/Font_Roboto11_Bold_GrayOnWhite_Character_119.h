
// created yyyy-mm-dd 

#ifndef Font_Roboto11_Bold_GrayOnWhite_Character_119_H
#define Font_Roboto11_Bold_GrayOnWhite_Character_119_H

#include "assets.h" 

extern const short int Font_Roboto11_Bold_GrayOnWhite_Character_119_x;
extern const short int Font_Roboto11_Bold_GrayOnWhite_Character_119_y;
extern const unsigned short Font_Roboto11_Bold_GrayOnWhite_Character_119[];
extern const image_t imageFont_Roboto11_Bold_GrayOnWhite_Character_119;


#endif


