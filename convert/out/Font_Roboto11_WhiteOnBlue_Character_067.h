
// created yyyy-mm-dd 

#ifndef Font_Roboto11_WhiteOnBlue_Character_067_H
#define Font_Roboto11_WhiteOnBlue_Character_067_H

#include "assets.h" 

extern const short int Font_Roboto11_WhiteOnBlue_Character_067_x;
extern const short int Font_Roboto11_WhiteOnBlue_Character_067_y;
extern const unsigned short Font_Roboto11_WhiteOnBlue_Character_067[];
extern const image_t imageFont_Roboto11_WhiteOnBlue_Character_067;


#endif


