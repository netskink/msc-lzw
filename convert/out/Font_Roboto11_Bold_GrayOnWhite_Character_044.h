
// created yyyy-mm-dd 

#ifndef Font_Roboto11_Bold_GrayOnWhite_Character_044_H
#define Font_Roboto11_Bold_GrayOnWhite_Character_044_H

#include "assets.h" 

extern const short int Font_Roboto11_Bold_GrayOnWhite_Character_044_x;
extern const short int Font_Roboto11_Bold_GrayOnWhite_Character_044_y;
extern const unsigned short Font_Roboto11_Bold_GrayOnWhite_Character_044[];
extern const image_t imageFont_Roboto11_Bold_GrayOnWhite_Character_044;


#endif


