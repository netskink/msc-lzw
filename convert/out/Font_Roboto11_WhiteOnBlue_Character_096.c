
// created yyyy-mm-dd 

#include "assets.h" 

const short int Font_Roboto11_WhiteOnBlue_Character_096_x = 5;
const short int Font_Roboto11_WhiteOnBlue_Character_096_y = 16;

const unsigned short Font_Roboto11_WhiteOnBlue_Character_096[] = {
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1f2e, 0xffff, 0x7eff, 0x1d55, 0x1d2d, 0x1d2d, 
0x1e2d, 0xffbf, 0xffff, 0x1d75, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
};


const image_t imageFont_Roboto11_WhiteOnBlue_Character_096 = {
	Font_Roboto11_WhiteOnBlue_Character_096_x,
	Font_Roboto11_WhiteOnBlue_Character_096_y,
	Font_Roboto11_WhiteOnBlue_Character_096
};


