#pragma once
void dump_string_buffer(char *pBuffer);
void dump_buffer(char *pBuffer, int len);
void byte_swap16(unsigned short int *pVal16);
void byte_swap32(unsigned int *pVal32);
int determine_rootname(char *pchFilename, char *pchRootname);
