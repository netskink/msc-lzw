
#include <stdio.h>
#include "version.h"


const int version_major = 0;
const int version_minor = 3;

void print_version(void) {
	printf("version %d.%d\n", version_major, version_minor);

}