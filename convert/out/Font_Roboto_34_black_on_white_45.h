
// created yyyy-mm-dd 

#ifndef Font_Roboto_34_black_on_white_45_H
#define Font_Roboto_34_black_on_white_45_H

#include "assets.h" 

extern const short int Font_Roboto_34_black_on_white_45_x;
extern const short int Font_Roboto_34_black_on_white_45_y;
extern const unsigned short Font_Roboto_34_black_on_white_45[];
extern const image_t imageFont_Roboto_34_black_on_white_45;


#endif


