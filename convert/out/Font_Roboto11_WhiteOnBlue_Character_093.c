
// created yyyy-mm-dd 

#include "assets.h" 

const short int Font_Roboto11_WhiteOnBlue_Character_093_x = 4;
const short int Font_Roboto11_WhiteOnBlue_Character_093_y = 16;

const unsigned short Font_Roboto11_WhiteOnBlue_Character_093[] = {
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x7f97, 0xffff, 0xffff, 0x9d95, 
0x1d2d, 0x1f77, 0xffff, 0x9d95, 0x1d2d, 0x1f77, 0xffff, 0x9d95, 
0x1d2d, 0x1f77, 0xffff, 0x9d95, 0x1d2d, 0x1f77, 0xffff, 0x9d95, 
0x1d2d, 0x1f77, 0xffff, 0x9d95, 0x1d2d, 0x1f77, 0xffff, 0x9d95, 
0x1d2d, 0x1f77, 0xffff, 0x9d95, 0x1d2d, 0x1f77, 0xffff, 0x9d95, 
0x1d2d, 0x1f77, 0xffff, 0x9d95, 0x1d2d, 0x1f77, 0xffff, 0x9d95, 
0x1d2d, 0x1f77, 0xffff, 0x9d95, 0x1d2d, 0x1f77, 0xffff, 0x9d95, 
0x7f97, 0xffff, 0xffff, 0x9d95, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
};


const image_t imageFont_Roboto11_WhiteOnBlue_Character_093 = {
	Font_Roboto11_WhiteOnBlue_Character_093_x,
	Font_Roboto11_WhiteOnBlue_Character_093_y,
	Font_Roboto11_WhiteOnBlue_Character_093
};


