
// created yyyy-mm-dd 

#ifndef Font_Roboto_40_white_on_gray_94_H
#define Font_Roboto_40_white_on_gray_94_H

#include "assets.h" 

extern const short int Font_Roboto_40_white_on_gray_94_x;
extern const short int Font_Roboto_40_white_on_gray_94_y;
extern const unsigned short Font_Roboto_40_white_on_gray_94[];
extern const image_t imageFont_Roboto_40_white_on_gray_94;


#endif


