#pragma once

// This file has two types of dictionaries
// Type One is:
//		o key as int
//		o value as string
//
// Type Two is:
//		o key as string
//		o value as int


// Type One hash table. This hash table has
//	o key is int
//	o value is char buffer
struct key_value_pair_int_key_str_val {
	unsigned int key; // use this field as the key
	unsigned char *pValue;  // use this field to store a variable length string
	int valueLen;
};


// Type one hash table
struct hash_table_int_key_str_value {
	unsigned int max;
	unsigned int number_of_elements;
	struct key_value_pair_int_key_str_val **elements;  // This is an array of pointers to mystruct objects
};


// Type Two hash table. This hash table has
// pKey is char buffer
// keyLen is len of char buffer
// keyHash is hash of string key
// value is int
struct key_value_pair_str_key_int_val {
	int keyHash; // this field is integer hash of the key
	unsigned char *pKey; // this field is the string key
	int keyLen; // since we no longer use strings, we now must store the len of the key.
	int value;  // this field is the int value
};

// Type two hash table
struct hash_table_str_key_int_value {
	int max;
	int number_of_elements;
	struct key_value_pair_str_key_int_val **elements;  // This is an array of pointers to mystruct objects
};


// common to each
unsigned int find_dict_slot(unsigned int key, unsigned int try_num, unsigned int max);
unsigned int FNV_hash(void* dataToHash, unsigned long int length);



// Type one hash table routines
int hash_insert(struct key_value_pair_int_key_str_val *data, struct hash_table_int_key_str_value *hash_table);
struct key_value_pair_int_key_str_val *hash_retrieve_1(unsigned int key, struct hash_table_int_key_str_value *hash_table);
int keys_1(struct hash_table_int_key_str_value *pHashTable, unsigned int **ppKeys);
int allocate_the_dictionary_intKey_strValue(struct hash_table_int_key_str_value *pHashTable);
int make_dict_entry_1(unsigned int a_key, unsigned char * buffer, int valueLen,  struct key_value_pair_int_key_str_val *pMyStruct);
int add_key_val_pair_to_dict_1(struct hash_table_int_key_str_value *pHashTable, unsigned int key, unsigned char *pValue, int valueLen);
void dump_hash_table_1(struct hash_table_int_key_str_value *pHashTable);
bool isKeyInDict_1(unsigned int key, struct hash_table_int_key_str_value *pHashTable);
int get_val_for_key_1(struct hash_table_int_key_str_value *pDict, unsigned int key, unsigned char **ppchResultValue, int *pValueLen);
void free_hash_table_1(struct hash_table_int_key_str_value *pHashTable);


// Type two hash table routines
//int hash_insert(struct key_value_pair_int_key_str_val *data, struct hash_table_int_key_str_value *hash_table);
//struct key_value_pair_int_key_str_val *hash_retrieve0(unsigned long int key, struct hash_table_int_key_str_value *hash_table);
struct key_value_pair_str_key_int_val *hash_retrieve_2(int keyHash, struct hash_table_str_key_int_value *hash_table);
int keys_2(struct hash_table_str_key_int_value *pHashTable, int **ppKeys);
int allocate_the_dictionary_strKey_intValue(struct hash_table_str_key_int_value *pHashTable);
int make_dict_entry_2(unsigned char *pKey, int lenKey, int value, struct key_value_pair_str_key_int_val *pKeyValuePair);
int add_key_val_pair_to_dict_2(struct hash_table_str_key_int_value *pHashTable, unsigned char *pKey, int lenKey, int value);
void dump_hash_table_2(struct hash_table_str_key_int_value *pHashTable);
bool isKeyInDict_2(unsigned char *pKey, int lenKey, struct hash_table_str_key_int_value *pHashTable);
int get_val_for_key_2(struct hash_table_str_key_int_value *pDict, unsigned char *pchKey, int lenKey, int *piResultValue);
void free_hash_table_2(struct hash_table_str_key_int_value *pHashTable);

