#!/bin/bash


# makes c header files using the jfd_convert.py script 
# makes one asset.h which includes all the generated files.

DATE=`date +%Y-%m-%d`


# write the .h file non variable part
echo "// Created $DATE " > out/assets.h
echo "" >> out/assets.h
echo "#ifndef ASSETS_H"  >> out/assets.h
echo "#define ASSETS_H"  >> out/assets.h

echo "struct image {"  >> out/assets.h
echo "	const unsigned short int x; "  >> out/assets.h
echo "	const unsigned short int y; "  >> out/assets.h
echo "	const unsigned short *pPixels; "  >> out/assets.h
echo "};"  >> out/assets.h
echo "" >> out/assets.h

echo "typedef struct image image_t;"  >> out/assets.h
echo "" >> out/assets.h


# write the .c file non variable part
echo "// Created $DATE " > out/raw_assets.c
echo "#include <stddef.h>" >> out/raw_assets.c
echo "#include \"assets.h\"" >> out/raw_assets.c
echo "" >> out/raw_assets.c
echo "" >> out/raw_assets.c


echo "const image_t *pImages[] = {"  >> out/raw_assets.c
echo "};"  >> out/raw_assets.c
echo "" >> out/raw_assets.c
echo "" >> out/raw_assets.c

echo "const image_t *pFontBoldGrayOnWhite[] = {"  >> out/raw_assets.c
echo "    NULL, // 00" >> out/raw_assets.c
echo "    NULL, // 01" >> out/raw_assets.c
echo "    NULL, // 02" >> out/raw_assets.c
echo "    NULL, // 03" >> out/raw_assets.c
echo "    NULL, // 04" >> out/raw_assets.c
echo "    NULL, // 05" >> out/raw_assets.c
echo "    NULL, // 06" >> out/raw_assets.c
echo "    NULL, // 07" >> out/raw_assets.c
echo "    NULL, // 08" >> out/raw_assets.c
echo "    NULL, // 09" >> out/raw_assets.c
echo "    NULL, // 10" >> out/raw_assets.c
echo "    NULL, // 11" >> out/raw_assets.c
echo "    NULL, // 12" >> out/raw_assets.c
echo "    NULL, // 13" >> out/raw_assets.c
echo "    NULL, // 14" >> out/raw_assets.c
echo "    NULL, // 15" >> out/raw_assets.c
echo "    NULL, // 16" >> out/raw_assets.c
echo "    NULL, // 17" >> out/raw_assets.c
echo "    NULL, // 18" >> out/raw_assets.c
echo "    NULL, // 19" >> out/raw_assets.c
echo "    NULL, // 20" >> out/raw_assets.c
echo "    NULL, // 21" >> out/raw_assets.c
echo "    NULL, // 22" >> out/raw_assets.c
echo "    NULL, // 23" >> out/raw_assets.c
echo "    NULL, // 24" >> out/raw_assets.c
echo "    NULL, // 25" >> out/raw_assets.c
echo "    NULL, // 26" >> out/raw_assets.c
echo "    NULL, // 27" >> out/raw_assets.c
echo "    NULL, // 28" >> out/raw_assets.c
echo "    NULL, // 29" >> out/raw_assets.c
echo "    NULL, // 30" >> out/raw_assets.c
echo "    NULL, // 31" >> out/raw_assets.c
echo "};"  >> out/raw_assets.c
echo "" >> out/raw_assets.c
echo "" >> out/raw_assets.c

echo "const image_t *pFontGrayOnWhite[] = {"  >> out/raw_assets.c
echo "    NULL, // 00" >> out/raw_assets.c
echo "    NULL, // 01" >> out/raw_assets.c
echo "    NULL, // 02" >> out/raw_assets.c
echo "    NULL, // 03" >> out/raw_assets.c
echo "    NULL, // 04" >> out/raw_assets.c
echo "    NULL, // 05" >> out/raw_assets.c
echo "    NULL, // 06" >> out/raw_assets.c
echo "    NULL, // 07" >> out/raw_assets.c
echo "    NULL, // 08" >> out/raw_assets.c
echo "    NULL, // 09" >> out/raw_assets.c
echo "    NULL, // 10" >> out/raw_assets.c
echo "    NULL, // 11" >> out/raw_assets.c
echo "    NULL, // 12" >> out/raw_assets.c
echo "    NULL, // 13" >> out/raw_assets.c
echo "    NULL, // 14" >> out/raw_assets.c
echo "    NULL, // 15" >> out/raw_assets.c
echo "    NULL, // 16" >> out/raw_assets.c
echo "    NULL, // 17" >> out/raw_assets.c
echo "    NULL, // 18" >> out/raw_assets.c
echo "    NULL, // 19" >> out/raw_assets.c
echo "    NULL, // 20" >> out/raw_assets.c
echo "    NULL, // 21" >> out/raw_assets.c
echo "    NULL, // 22" >> out/raw_assets.c
echo "    NULL, // 23" >> out/raw_assets.c
echo "    NULL, // 24" >> out/raw_assets.c
echo "    NULL, // 25" >> out/raw_assets.c
echo "    NULL, // 26" >> out/raw_assets.c
echo "    NULL, // 27" >> out/raw_assets.c
echo "    NULL, // 28" >> out/raw_assets.c
echo "    NULL, // 29" >> out/raw_assets.c
echo "    NULL, // 30" >> out/raw_assets.c
echo "    NULL, // 31" >> out/raw_assets.c
echo "};"  >> out/raw_assets.c
echo "" >> out/raw_assets.c
echo "" >> out/raw_assets.c




echo "const image_t *pFontWhiteOnBlue[] = {"  >> out/raw_assets.c
echo "    NULL, // 00" >> out/raw_assets.c
echo "    NULL, // 01" >> out/raw_assets.c
echo "    NULL, // 02" >> out/raw_assets.c
echo "    NULL, // 03" >> out/raw_assets.c
echo "    NULL, // 04" >> out/raw_assets.c
echo "    NULL, // 05" >> out/raw_assets.c
echo "    NULL, // 06" >> out/raw_assets.c
echo "    NULL, // 07" >> out/raw_assets.c
echo "    NULL, // 08" >> out/raw_assets.c
echo "    NULL, // 09" >> out/raw_assets.c
echo "    NULL, // 10" >> out/raw_assets.c
echo "    NULL, // 11" >> out/raw_assets.c
echo "    NULL, // 12" >> out/raw_assets.c
echo "    NULL, // 13" >> out/raw_assets.c
echo "    NULL, // 14" >> out/raw_assets.c
echo "    NULL, // 15" >> out/raw_assets.c
echo "    NULL, // 16" >> out/raw_assets.c
echo "    NULL, // 17" >> out/raw_assets.c
echo "    NULL, // 18" >> out/raw_assets.c
echo "    NULL, // 19" >> out/raw_assets.c
echo "    NULL, // 20" >> out/raw_assets.c
echo "    NULL, // 21" >> out/raw_assets.c
echo "    NULL, // 22" >> out/raw_assets.c
echo "    NULL, // 23" >> out/raw_assets.c
echo "    NULL, // 24" >> out/raw_assets.c
echo "    NULL, // 25" >> out/raw_assets.c
echo "    NULL, // 26" >> out/raw_assets.c
echo "    NULL, // 27" >> out/raw_assets.c
echo "    NULL, // 28" >> out/raw_assets.c
echo "    NULL, // 29" >> out/raw_assets.c
echo "    NULL, // 30" >> out/raw_assets.c
echo "    NULL, // 31" >> out/raw_assets.c
echo "};"  >> out/raw_assets.c
echo "" >> out/raw_assets.c
echo "" >> out/raw_assets.c


echo "const image_t *pFontWhiteOnGray[] = {"  >> out/raw_assets.c
echo "};"  >> out/raw_assets.c
echo "" >> out/raw_assets.c
echo "" >> out/raw_assets.c


for filename in input_images/*.bmp; do

	echo $filename
	./msc-lzw.exe -m RG_GB -V -d out -n $filename
	echo "#include \"$(basename ${filename%.*}).h\"" >> out/assets.h
	echo "    &image$(basename ${filename%.*})," >> out/raw_assets.c

done

# these will have to be adjusted manually in the assets.c filename
echo "" >> out/assets.h
echo "" >> out/assets.h
echo "extern const image_t *pImages[];" >> out/assets.h
echo "extern const image_t *pFontGrayOnWhite[];" >> out/assets.h
echo "extern const image_t *pFontBoldGrayOnWhite[];" >> out/assets.h
echo "extern const image_t *pFontWhiteOnBlue[];" >> out/assets.h
echo "extern const image_t *pFontWhiteOnGray[];" >> out/assets.h
echo "" >> out/assets.h
echo "" >> out/assets.h




echo "#endif" >> out/assets.h

echo "" >> out/assets.h
echo "" >> out/assets.h
echo "" >> out/raw_assets.c
echo "" >> out/raw_assets.c


