#pragma pack(1)
#pragma once

#include "bitmap.h"
#include "main.h"

#include <iostream>
//#include <unistd.h>
#include <fstream>


// stolen from 
// https://stackoverflow.com/questions/5751749/how-can-i-read-bmp-pixel-values-into-an-array

// msdn ref on bitmaps
// https://msdn.microsoft.com/en-us/library/dd183377(v=vs.85).aspx


// some notes. pixels are reversed. Bottom of image appears first in file.

using std::cout;
using std::endl;
using std::ofstream;
using std::ifstream;


typedef int LONG;
typedef unsigned short WORD;
typedef unsigned int DWORD;






// I can't just use the buffer, because its:
//    o a block of 16-bit words (if bit size is 16 bits)
//    o flipped. top of image on display is bottom of memory. If its a core header and the value is negative, its stored non-flipped.
//    <verify this> o its little endian machine so its ordered <LSB0> <MSB0>, <LSB1> <MSB1>,  <LSB2> <MSB2>, ...
//    o  Also, the buffer is left to right but my arrays in the C code are ordered as show on the screen.  So the top left pixel is stored first.
//       the lower right pixel is stored last.
//     
//       The buffer as I am parsing byte by byte is top to bottom, but its not so simple to reverse it because it needs to be starting at last row
//       first column.  
// 
// Currently setup for row order
// The output byte array is written like this
// Top to bottom, left to right
// starting with top left display pixel
//
// RGB565
// ----------
// rgb16_byte_order = RG_GB 
//   <16-bit pixel> = <RedGreen><GreenBblue>  <RedGreen><GreenBlue>  ...
//
// This can vary to be like
// rgb16_byte_order = GB_RG
//   <16-bit pixel> = <GreenBblue><RedGreen>  <GreenBlue><RedGreen>  ...
//

// output from the python code produces the follow output.  This is in RG_GB order
//		const short int test_0_8x4_rgb16_x = 8;
//		const short int test_0_8x4_rgb16_y = 4;
//
//		const unsigned short test_0_8x4_rgb16[] = {
//			0xf800 , 0xffff , 0xf800 , 0xffff , 0xf800 , 0xffff , 0xf800 , 0xffff ,
//			0xffff , 0xffff , 0xffff , 0xffff , 0xffff , 0xffff , 0xffff , 0xffff ,
//			0xffff , 0xffff , 0xffff , 0xffff , 0xffff , 0xffff , 0xffff , 0xffff ,
//			0xf800 , 0x07e0 , 0x001f , 0xffff , 0xffff , 0xffff , 0xffff , 0x0000
//		};
//
//

// This was for 16-bit to 16-bit.  These also have the problem where they are
// padded to four byte boundaries.

#ifdef does_not_work_for_padded_right_sides
int ExtractImageArray_rgb565_to_rgb565(unsigned short int *pBufferIn, int iBufLenAsBytes,
	                                   unsigned int uiRedMask, unsigned int uiGreenMask, unsigned int uiBlueMask, 
	                                   unsigned char *pchBufferOut,
	                                   rgb16_byte_order_t eByteOrder,
							           int nRows, int nCols) {
	
	int i;

	unsigned int uiRed;
	unsigned int uiGreen;
	unsigned int uiBlue;
	unsigned short int *pPixel;
	unsigned short int uiPixel;
	int buffLenAsShorts;
	int offset;
	int iRow;
	int iCol;


	// Testing against the python code, it shows that this buffer is reversed.
	// But there is something else up.  Perhaps it is also row and column in memory?

	/* for a 16-bit pixel, the pixels are stored as 16-bit pixels in the following format
	   ie. flipped from display, bottom to top, left to right

	   <lower left pixel=last row, first column> <same row, one column to right> .... <last row, last column>
	   <next to last row, first column> < same row, one column to right>
		...
		<top right pixel=first row, first column> <first row, second column> ... <first row, last column>
	
	*/

	i = 0;
	iRow = nRows - 1;
	iCol = 0;
	pPixel = pBufferIn;
	buffLenAsShorts = iBufLenAsBytes / 2;
	while (i < buffLenAsShorts) {

		uiPixel = *pPixel;
		
		uiRed = uiPixel & uiRedMask;
		uiRed = uiRed >> ( 6 + 5 );
		uiGreen = uiPixel & uiGreenMask;
		uiGreen = uiGreen >> ( 5 );
		uiBlue = uiPixel & uiBlueMask;
		uiBlue = uiBlue >> ( 0 );
//		printf("buffIn[%d] = %#6x\n", i, uiPixel);
//		printf("\tred = %#x\n", uiRed);
//		printf("\tgreen = %#x\n", uiGreen);
//		printf("\tblue = %#x\n", uiBlue);

		//
		// write to output buffer as flipped top to bottom
		// (This is not what the python code is showing.  For a first pass
		// lets mimic i completely.
		//
		offset = (iRow) * nCols * 2 + ( iCol ) * 2;

		iCol++;
		if (iCol == nCols) {
			iCol = 0;
			iRow = iRow - 1;

		}

		if (RG_GB == eByteOrder) {

			// Write RG GB byte order

			pchBufferOut[offset] = ( uiRed << 3 ) | ( uiGreen >> 3 );
			pchBufferOut[offset + 1] = ( uiGreen << 5 ) | uiBlue;

		} else {

			// Write GB RG byte order
			pchBufferOut[offset + 1] = ( uiRed << 3 ) | ( uiGreen >> 3 );
			pchBufferOut[offset] = ( uiGreen << 5 ) | uiBlue;

		}


		// increment the pixel pointer and short count
		pPixel++;
		i++;
	}

	return RC_OK;

}

#endif


int ExtractImageArray_rgb565_to_rgb565(unsigned char *pBufferIn, int iBufLenAsBytes,
	                                   unsigned int uiRedMask, unsigned int uiGreenMask, unsigned int uiBlueMask,
	                                   unsigned char *pchBufferOut,
	                                   rgb16_byte_order_t eByteOrder,
	                                   int nRows, int nCols) {



	// Testing against the python code, it shows that this buffer is reversed.
	// But there is something else up.  Perhaps it is also row and column in memory?

	/* for a 16-bit pixel, the pixels are stored as 16-bit pixels in the following format
	ie. flipped from display, bottom to top, left to right

	<lower left pixel=last row, first column> <same row, one column to right> .... <last row, last column>
	<next to last row, first column> < same row, one column to right>
	...
	<top right pixel=first row, first column> <first row, second column> ... <first row, last column>

	*/

	////////////////////////////////////

	unsigned char *pPixel;
	int offset;

	// Use this to iterate pixels by row and column
	int iRow;
	int iCol;
	int nColsPad;

	unsigned short int usiRed;
	unsigned short int usiGreen;
	unsigned short int usiBlue;


	unsigned char uchRed;
	unsigned char uchGreen;
	unsigned char uchBlue;

	unsigned short int usiPixel;

	// Pad row size to a multiple of 4 bytes
	// pad n size = (~n + 1) & (size -1)
	// pad 13 8 = 3
	// pad 11 4 = 1
	//nColsPad = ( ~nCols + 1 ) & ( 4 - 1 );  // pad on 4 bytes
	nColsPad = ( ~nCols + 1 ) & ( 2 - 1 );  // pad on 2 bytes
	nColsPad = nColsPad + nCols;

	pPixel = pBufferIn;
	for (iRow = nRows - 1; iRow >= 0; iRow--) {
		for (iCol = 0; iCol < nColsPad; iCol++) {
			if (iCol >= nCols) {
				pPixel++;
				pPixel++;
				continue;
			}

			usiPixel = *(unsigned short int *)pPixel;

			usiRed = usiPixel & uiRedMask;
			usiGreen = usiPixel & uiGreenMask;
			usiBlue = usiPixel & uiBlueMask;


			uchRed = usiRed >> ( 8+3);
			uchGreen = usiGreen >> ( 3+2 );
			uchBlue = usiBlue >> ( 0 );


										//
										// write to output buffer as flipped top to bottom
										// (This is not what the python code is showing.  For a first pass
										// lets mimic i completely.
										//
										// offset for output  buffer
			offset = (iRow) * nCols * 2 + ( iCol ) * 2;


			if (RG_GB == eByteOrder) {

				// Write RG GB byte order
				pchBufferOut[offset] = (uchRed << 3) | ( uchGreen >> 3 );  // shift_r 3 to get portion in RG byte 
				pchBufferOut[offset + 1] = ( uchGreen << 5 ) | ( uchBlue >> 0 );  // shift_l 5 to leave room for blue. 


			} else {

				// Write GB RG byte order
				pchBufferOut[offset + 1] = uchRed | ( uchGreen >> 5 );  // shift_r 2 to make it 6 bits. shift_r 3 to get portion in RG byte 
				pchBufferOut[offset] = ( uchGreen << 3 ) | ( uchBlue >> 3 );  // shift_r 2 bits to make it 6 bits, then shift_l 5 to leave room for blue. shift blue down 3 to get 5 msb bits

			}

			pPixel++;
			pPixel++;
		}
	}

	////////////////////////////////////
	return RC_OK;

}



/* for a 24-bit pixel, the pixels are stored as 24-bit pixels in the following format
ie. flipped from display, bottom to top, left to right.  They appear to be 
<blue byte> <green byte> <red byte>,

<lower left pixel=last row, first column> <same row, one column to right> .... <last row, last column>
<next to last row, first column> < same row, one column to right>
...
<top right pixel=first row, first column> <first row, second column> ... <first row, last column>

*/

// Furthermore these pixels are padded to multiple of 4 byte widths

// These bytes are padded to multiple of 4 byte widths
// a 11x16 image is padded to 12x16
// 3*16=48
// 48 is the differnece in bytes of 11x16x3 and reported image size.

int ExtractImageArray_rgb888_to_rgb565(unsigned char *pBufferIn, int iBufLenAsBytes,
	                                   unsigned char *pchBufferOut,
	                                   rgb16_byte_order_t eByteOrder,
	                                   int nRows, int nCols,
			                           int pad_size) {


	unsigned char *pPixel;
	int offset;

	// Use this to iterate pixels by row and column
	int iRow;
	int iCol;
	int nColsPad;
	int pad_size_test;

	unsigned char uchRed;
	unsigned char uchGreen;
	unsigned char uchBlue;


	// this works to up the number of columns, but the already
	// calculated pad size is correct for the number of bytes.
	// 
	// Pad row size to a multiple of 4 bytes
	// pad n size = (~n + 1) & (size -1)
	// pad 13 8 = 3
	// pad 11 4 = 1
	// nColsPad = ( ~nCols + 1 ) & ( 4 - 1 );
	//	nColsPad = nColsPad*3 + nCols;  // this is number of cols, so incr pixels by 3
	// pad_size_test = nCols + pad_size;
	// if (pad_size_test != nColsPad) {
	// 	printf("could be an error with padding.  Check why.\n");
	//		nColsPad = nCols + pad_size/3;
	// }
	nColsPad = nCols + pad_size;  // this is in terms of bytes so incr pixels once

	pPixel = pBufferIn;
	for (iRow = nRows-1; iRow >= 0; iRow--) {
		for (iCol = 0; iCol < nColsPad; iCol++) {
			if (iCol >= nCols) {
				pPixel++;  // the cols count in terms of pixels, but this cols pad is in terms of bytes.
//				pPixel++;
//				pPixel++;
				continue;
			}


			uchBlue = pPixel[0];  // 8 bits
			uchGreen = pPixel[1]; // 8 bits
			uchRed = pPixel[2];   // 8 bits

			uchRed = uchRed & 0xF8;     // 5 msb bits
			uchGreen = uchGreen & 0xFC; // 6 msb bits
			uchBlue = uchBlue & 0xF8;   // 5 msb bits


										//
										// write to output buffer as flipped top to bottom
										// (This is not what the python code is showing.  For a first pass
										// lets mimic i completely.
										//
										// offset for output  buffer
			offset = (iRow) * nCols * 2 + ( iCol ) * 2;


			if (RG_GB == eByteOrder) {

				// Write RG GB byte order
				pchBufferOut[offset] = uchRed | ( uchGreen >> 5 );  // shift_r 2 to make it 6 bits. shift_r 3 to get portion in RG byte 
				pchBufferOut[offset + 1] = ( uchGreen << 3 ) | ( uchBlue >> 3 );  // shift_r 2 bits to make it 6 bits, then shift_l 5 to leave room for blue. shift blue down 3 to get 5 msb bits


			} else {

				// Write GB RG byte order
				pchBufferOut[offset + 1] = uchRed | ( uchGreen >> 5 );  // shift_r 2 to make it 6 bits. shift_r 3 to get portion in RG byte 
				pchBufferOut[offset] = ( uchGreen << 3 ) | ( uchBlue >> 3 );  // shift_r 2 bits to make it 6 bits, then shift_l 5 to leave room for blue. shift blue down 3 to get 5 msb bits

			}

			pPixel++;
			pPixel++;
			pPixel++;


		}
	}


	return RC_OK;

}


int ParseV1Header(char*& buffer, unsigned int& BufferSize, int &nRows, int &nCols, rgb16_byte_order_t eByteOrder, unsigned char *&buffer_out, unsigned int & buffer_out_size) {

	PMYBITMAPFILEHEADER file_header;
	PMYBITMAPV1INFOHEADER info_header_v1;
	unsigned char *buf_in;
	unsigned char *pColors;
	int pad_size;

	file_header = (PMYBITMAPFILEHEADER) ( &buffer[0] );
	info_header_v1 = (PMYBITMAPV1INFOHEADER) ( &buffer[0] + sizeof(MYBITMAPFILEHEADER) );

	nRows = info_header_v1->iHeight;
	nCols = info_header_v1->iWidth;
	BufferSize = file_header->uiSize;

	// Dump the info from the headers
	cout << "Width = " << nCols << endl;
	cout << "Height = " << nRows << endl;
	cout << "Offset = " << std::hex << file_header->uiOffBits << endl;
	cout << "Number of bits per pixel = " << std::dec << info_header_v1->usiBitCount << endl;
	cout << "Compression method used = " << info_header_v1->uiCompression << endl;
	cout << "Header size = " << info_header_v1->uiSize << endl;
	cout << "image size vs bits used and padding debug "<<  endl;
	cout << "---------------------" << endl;
	cout << "rows*cols*(bits_per_pixel/8) = " << std::dec << (nCols * nRows*info_header_v1->usiBitCount)/8 << endl;
	cout << "image array size = " << std::dec << info_header_v1->uiSizeImage << endl;
	pad_size = ( ( info_header_v1->uiSizeImage ) - ( ( nCols * nRows*info_header_v1->usiBitCount ) / 8 ) ) / nRows;
	cout << "col padding (in terms of bytes) = " << std::dec << pad_size << endl;

	if (MYBI_RGB == info_header_v1->uiCompression && 24 == info_header_v1->usiBitCount) {
		// parse the buffer
		cout << "parsing via V1 header" << endl;
		buf_in = (unsigned char *) &buffer[0] + file_header->uiOffBits;  // The start of the image array. uiOffBits is offset in therms of bytes from beginning of bit map file header
		pColors = (unsigned char *) &buffer[0] + info_header_v1->uiSize;
		int buf_sizes = info_header_v1->uiSizeImage; // It pads the bytes to multiples of 4 bytes for the columns
		buffer_out_size = (unsigned int) buf_sizes;
		buffer_out_size = nRows * nCols * 2;  // the buffer out is not as big as the input buffer
		buffer_out = (unsigned char *) malloc(buf_sizes); // go ahead and allocate more than required. fix it up afterwards.
		// going into the routine the buffer is 3 bytes for each pixel.
		// a 8x4 img will have 8*4*3=96 bytes or 0x60 bytes. --> buf_sizes=0x60
		// coming out, the buffer is 16-bit RGB so it will be 8*4*2=64 or 0x40
		ExtractImageArray_rgb888_to_rgb565(buf_in, buf_sizes, buffer_out, eByteOrder, nRows, nCols, pad_size);


	} else {
		printf("currently only supports v1 headers with no compression and 24bit\n");
		return RC_ERROR;
	}
	cout << "buffer out size for image array = " << std::dec << buffer_out_size << endl;

	return RC_OK;
}


int ParseV4Header(char*& buffer, unsigned int& BufferSize, int &nRows, int &nCols, rgb16_byte_order_t eByteOrder, unsigned char *&buffer_out, unsigned int & buffer_out_size) {

	PMYBITMAPFILEHEADER file_header;
	PMYBITMAPV4INFOHEADER info_header_v4;
	unsigned char *buf_in;
	unsigned char *pColors;
	int pad_size;

	file_header = (PMYBITMAPFILEHEADER) ( &buffer[0] );
	info_header_v4 = (PMYBITMAPV4INFOHEADER) ( &buffer[0] + sizeof(MYBITMAPFILEHEADER) );

	nRows = info_header_v4->iHeight;
	nCols = info_header_v4->iWidth;
	BufferSize = file_header->uiSize;

	// Dump the info from the headers
	cout << "Width = " << nCols << endl;
	cout << "Height = " << nRows << endl;
	cout << "Offset = " << std::hex << file_header->uiOffBits << endl;
	cout << "Number of bits per pixel = " << std::dec << info_header_v4->usiBitCount << endl;
	cout << "Compression method used = " << info_header_v4->uiCompression << endl;
	cout << "Header size = " << info_header_v4->uiSize << endl;
	cout << "image size vs bits used and padding debug " << endl;
	cout << "---------------------" << endl;
	cout << "rows*cols*(bits_per_pixel/8) = " << std::dec << ( nCols * nRows*info_header_v4->usiBitCount ) / 8 << endl;
	cout << "image array size = " << std::dec << info_header_v4->uiSizeImage << endl;
	pad_size = ( ( info_header_v4->uiSizeImage ) - ( ( nCols * nRows*info_header_v4->usiBitCount ) / 8 ) ) / nRows;
	cout << "col padding = " << std::dec << pad_size << endl;

	if (MYBI_RGB == info_header_v4->uiCompression && 24 == info_header_v4->usiBitCount) {
		// parse the buffer
		cout << "parsing via V4 header" << endl;
		buf_in = (unsigned char *) &buffer[0] + file_header->uiOffBits;  // The start of the image array. uiOffBits is offset in therms of bytes from beginning of bit map file header
																		 //		buf_in = (unsigned char *) &buffer[0] + file_header->uiOffBits+48;  // The start of the image array. uiOffBits is offset in therms of bytes from beginning of bit map file header
																		 //		buf_in = (unsigned char *) &buffer[0] + file_header->uiOffBits-48;  // The start of the image array. uiOffBits is offset in therms of bytes from beginning of bit map file header
		pColors = (unsigned char *) &buffer[0] + info_header_v4->uiSize;
		int buf_sizes = info_header_v4->uiSizeImage; // It pads the bytes to multiples of 4 bytes for the columns
		buffer_out_size = (unsigned int) buf_sizes;
		buffer_out = (unsigned char *) malloc(buf_sizes); // go ahead and allocate more than required. fix it up afterwards.
														  // going into the routine the buffer is 3 bytes for each pixel.
														  // a 8x4 img will have 8*4*3=96 bytes or 0x60 bytes. --> buf_sizes=0x60
														  // coming out, the buffer is 16-bit RGB so it will be 8*4*2=64 or 0x40
		ExtractImageArray_rgb888_to_rgb565(buf_in, buf_sizes, buffer_out, eByteOrder, nRows, nCols, pad_size);
		// fix it up here. who cares if the buffer is to big going in.
		buffer_out_size = nRows * nCols * 2;
	} else	if (MYBI_BITFIELDS == info_header_v4->uiCompression && 16 == info_header_v4->usiBitCount) {
		// parse the buffer
		cout << "parsing via V4 header" << endl;
		buf_in = (unsigned char *) &buffer[0] + file_header->uiOffBits;  // The start of the image array. uiOffBits is offset in therms of bytes from beginning of bit map file header
		int buf_sizes = info_header_v4->uiSizeImage;
		unsigned int uiRedMask = info_header_v4->uiRedMask;
		unsigned int uiGreenMask = info_header_v4->uiGreenMask;
		unsigned int uiBlueMask = info_header_v4->uiBlueMask;
		buffer_out_size = nRows * nCols * 2;
		buffer_out = (unsigned char *) malloc(buffer_out_size);
		ExtractImageArray_rgb565_to_rgb565(buf_in, buf_sizes, uiRedMask, uiGreenMask, uiBlueMask, buffer_out, eByteOrder, nRows, nCols);
		// fix it up here. who cares if the buffer is to big going in.
	}



	return RC_OK;
}



int ParseV5Header( char*& buffer, unsigned int& BufferSize, int &nRows, int &nCols, rgb16_byte_order_t eByteOrder, unsigned char *&buffer_out, unsigned int & buffer_out_size) {

	PMYBITMAPFILEHEADER file_header;
	PMYBITMAPV5INFOHEADER info_header_v5;
	unsigned char *buf_in;
	int pad_size;

	file_header = (PMYBITMAPFILEHEADER) ( &buffer[0] );
	info_header_v5 = (PMYBITMAPV5INFOHEADER) ( &buffer[0] + sizeof(MYBITMAPFILEHEADER) );

	nRows = info_header_v5->iHeight;
	nCols = info_header_v5->iWidth;
	BufferSize = file_header->uiSize;

	// Dump the info from the headers
	cout << "Width = " << nCols << endl;
	cout << "Height = " << nRows << endl;
	cout << "Offset = " << std::hex << file_header->uiOffBits << endl;
	cout << "Number of bits per pixel = " << std::dec << info_header_v5->usiBitCount << endl;
	cout << "Compression method used = " << info_header_v5->uiCompression << endl;
	cout << "Header size = " << info_header_v5->uiSize << endl;
	cout << "image size vs bits used and padding debug " << endl;
	cout << "---------------------" << endl;
	cout << "rows*cols*(bits_per_pixel/8) = " << std::dec << ( nCols * nRows*info_header_v5->usiBitCount ) / 8 << endl;
	cout << "image array size = " << std::dec << info_header_v5->uiSizeImage << endl;
	pad_size = ( ( info_header_v5->uiSizeImage ) - ( ( nCols * nRows*info_header_v5->usiBitCount ) / 8 ) ) / nRows;
	cout << "col padding = " << std::dec << pad_size << endl;

	if (MYBI_BITFIELDS == info_header_v5->uiCompression && 24 == info_header_v5->usiBitCount) {
		// parse the buffer
		cout << "parsing via V5 header" << endl;
		buf_in = (unsigned char *) &buffer[0] + file_header->uiOffBits;  // The start of the image array. uiOffBits is offset in therms of bytes from beginning of bit map file header
		int buf_sizes = info_header_v5->uiSizeImage;
		unsigned int uiRedMask = info_header_v5->uiRedMask;
		unsigned int uiGreenMask = info_header_v5->uiGreenMask;
		unsigned int uiBlueMask = info_header_v5->uiBlueMask;
		// fix it up here. who cares if the buffer is to big going in.
		buffer_out_size = nRows * nCols * 2;
		buffer_out = (unsigned char *) malloc(buffer_out_size);
		ExtractImageArray_rgb888_to_rgb565(buf_in, buf_sizes, buffer_out, eByteOrder, nRows, nCols, pad_size);
	} else	if (MYBI_BITFIELDS == info_header_v5->uiCompression && 16 == info_header_v5->usiBitCount) {
		// parse the buffer
		cout << "parsing via V5 header" << endl;
		buf_in = (unsigned char *) &buffer[0] + file_header->uiOffBits;  // The start of the image array. uiOffBits is offset in therms of bytes from beginning of bit map file header
		int buf_sizes = info_header_v5->uiSizeImage;
		unsigned int uiRedMask = info_header_v5->uiRedMask;
		unsigned int uiGreenMask = info_header_v5->uiGreenMask;
		unsigned int uiBlueMask = info_header_v5->uiBlueMask;
		buffer_out_size = nRows * nCols * 2;
		buffer_out = (unsigned char *) malloc(buffer_out_size);
		ExtractImageArray_rgb565_to_rgb565(buf_in, buf_sizes, uiRedMask, uiGreenMask, uiBlueMask, buffer_out, eByteOrder, nRows, nCols);
		// fix it up here. who cares if the buffer is to big going in.
	}
	return RC_OK;
}



// This is routine from the stack overflow post.
// I'm using a modified version to determine the file header information
int FillAndAllocate( const char* FileName, unsigned char*& buffer_out, unsigned int& BufferSize, rgb16_byte_order_t eByteOrder, int &nRows, int &nCols) { 

	std::ifstream file((char *) FileName);
	char *buffer;
	unsigned int buffer_out_size;  // there are two buffers, the buffer size for parsing and the buffersize of the image pixel array

	if (file) {
		file.seekg(0, std::ios::end);
		std::streampos length = file.tellg();
		file.seekg(0, std::ios::beg);

		buffer = new char[length];
		file.read(&buffer[0], length);

		PMYBITMAPFILEHEADER file_header;
		PMYBITMAPINFOHEADER info_header_guess;

		// Really you should examine the header size and typecast this info 
		// based upon it, but this routine uses the core bitmap info header so
		// no need.


		file_header = (PMYBITMAPFILEHEADER) ( &buffer[0] );
		info_header_guess = (PMYBITMAPINFOHEADER) ( &buffer[0] + sizeof(MYBITMAPFILEHEADER) );


		if (124 == info_header_guess->uiSize) {
			// This is a V5 InfoHeader
			ParseV5Header(buffer, BufferSize, nRows, nCols, eByteOrder, buffer_out, buffer_out_size);
			BufferSize = buffer_out_size;

		} else if (56 == info_header_guess->uiSize) {
			// This is a V3 InfoHeader
			// I get this with gimp if I do not write color space info
			return RC_ERROR;

		} else if (40 == info_header_guess->uiSize) {
			// This is a V1 InfoHeader
			// I get this if I use windows10 paint program to write a bitmap
			ParseV1Header(buffer, BufferSize, nRows, nCols, eByteOrder, buffer_out, buffer_out_size);
			BufferSize = buffer_out_size;

		} else if (108 == info_header_guess->uiSize) {
			// This is a V1 InfoHeader
			// I get this if I use windows10 paint program to write a bitmap
			ParseV4Header(buffer, BufferSize, nRows, nCols, eByteOrder, buffer_out, buffer_out_size);
			BufferSize = buffer_out_size;

		} else {
			// Currently unsupported header type
			cout << "File" << FileName << " uses unsupported header type." << endl;
			cout << "Current supported types are: v5, v4, v3, and v1." << endl;
			return RC_ERROR;
		}


		delete buffer;  // this is the overall file buffer.
		// buffer_out is the image pixel array as bytes and it is returned to the caller.
		return RC_OK;
	} else {
		cout << "File" << FileName << " don't Exist!" << endl;
		return RC_ERROR;
	}
}



// This is a routine from the stack overflow post.
// I'm currently not using it.
#ifdef notused
void ColorTest() {

	// Makes Red Rectangle in top left corner. Rectangle stretches to right alot
	for (int i = rows / 10; i < 3 * rows / 10; i++)
		for (int j = cols / 10; j < 7 * cols / 10; j++)
			reds[i][j] = 0xff;

	// Makes small green box in bottom right
	for (int i = 8 * rows / 10; i < rows; i++)
		for (int j = 8 * cols / 10; j < cols; j++)
			greens[i][j] = 0xff;

	// Makes White box in the middle of the screeene    
	for (int i = rows * 4 / 10; i < rows * 6 / 10; i++)
		for (int j = cols * 4 / 10; j < cols * 6 / 10; j++) {
			greens[i][j] = 0xff;
			reds[i][j] = 0xff;
			blues[i][j] = 0xff;
		}

	// Blue verticle rectangle bottom left
	for (int i = rows * 6 / 10; i < rows; i++)
		for (int j = cols * 0; j < cols * 1 / 10; j++)
			blues[i][j] = 0xff;
}

//  This is a routine from the stack overflow post.
// I'm currently not using it.
void RGB_Allocate(unsigned char**& dude) {

	dude = new unsigned char*[rows];
	for (int i = 0; i < rows; i++)
		dude[i] = new unsigned char[cols];
}
#endif



// This is a routine lifted from the stackoverflow post.
// i'm currently not using it.
#ifdef unused
void GetPixlesFromBMP24(unsigned char** reds, unsigned char** greens, unsigned char** blues, int end, int rows, int cols, char* FileReadBuffer) { // end is BufferSize (total size of file)
	int count = 1;
	int extra = cols % 4; // The nubmer of bytes in a row (cols) will be a multiple of 4.
	for (int i = 0; i < rows; i++) {
		count += extra;
		for (int j = cols - 1; j >= 0; j--)
			for (int k = 0; k < 3; k++) {
				switch (k) {
				case 0:
					reds[i][j] = FileReadBuffer[end - count++];
					break;
				case 1:
					greens[i][j] = FileReadBuffer[end - count++];
					break;
				case 2:
					blues[i][j] = FileReadBuffer[end - count++];
					break;
				}
			}
	}
}


// This is a routine lifted from the stackoverflow post.
// I'm currently not using it.
void WriteOutBmp24(char* FileBuffer, const char* NameOfFileToCreate, int BufferSize) {
	std::ofstream write(NameOfFileToCreate);
	if (!write) {
		cout << "Failed to write " << NameOfFileToCreate << endl;
		return;
	}
	int count = 1;
	int extra = cols % 4; // The nubmer of bytes in a row (cols) will be a multiple of 4.
	for (int i = 0; i < rows; i++) {
		count += extra;
		for (int j = cols - 1; j >= 0; j--)
			for (int k = 0; k < 3; k++) {
				switch (k) {
				case 0: //reds
					FileBuffer[BufferSize - count] = reds[i][j];
					break;
				case 1: //green
					FileBuffer[BufferSize - count] = greens[i][j];
					break;
				case 2: //blue
					FileBuffer[BufferSize - count] = blues[i][j];
					break;
				}
				count++;
			}
	}
	write.write(FileBuffer, BufferSize);
}
#endif


#ifdef TEST_BITMAP

int test_bitmap(const char *pchFilename) {

	unsigned char* FileBuffer; 
	unsigned int BufferSize;
	bool bRC;
	int nRows, nCols;


	cout << "File name is " << pchFilename << endl;
	bRC = FillAndAllocate( pchFilename, FileBuffer, BufferSize, RG_GB, nRows, nCols);
	if (RC_ERROR == bRC) {
		cout << "File read error" << endl; return 0; 
	}
	cout << "Rows: " << nRows << " Cols: " << nCols << endl;

	//RGB_Allocate(reds);
	//RGB_Allocate(greens);
	//RGB_Allocate(blues);
	//GetPixlesFromBMP24(reds, greens, blues, BufferSize, rows, cols, FileBuffer);
	//ColorTest();

//#define WriteOutFile "OutputPicture.bmp"
//	WriteOutBmp24(FileBuffer, WriteOutFile, BufferSize);
	return RC_OK;
}

int main(int argc, char *argv[]) {



	//  file name is test-0-16x7-rgb16.bmp
	// File was created with gip and exported with the following options
	// no rle compression
	// write color space
	// rgb16
	//
	//	Width = 16
	//	Height = 7
	//	Offset = 0x8a
	//	Number of bits per pixel = 16
	//	Compression method used = 3
	// imagemagick says this is ?
	const char filename0[100] = { "test_0_8x4_rgb16.bmp" };



	//  file name is test-0-16x7-rgb16.bmp
	// File was created with gip and exported with the following options
	// no rle compression
	// write color space
	// rgb16
	//
	//	Width = 16
	//	Height = 7
	//	Offset = 0x8a
	//	Number of bits per pixel = 16
	//	Compression method used = 3
	// imagemagick says this is ?
	const char filename1[100] = { "test-0-16x7-rgb16.bmp" };





	//  file name is BlueBattery.bmp
	//	Width = 45
	//	Height = 81
	//	Offset = 36
	//	Number of bits per pixel = 24
	//	Compression method used = 0
	// imagemagick says this is a v3 header
	const char filename11[100] = { "BlueBattery.bmp" };


	//  File name is rgb32x32.bmp
	//	Width = 32
	//	Height = 32
	//	Offset = 8a
	//	Number of bits per pixel = 16
	//	Compression method used = 3
	//	Rows: 20 Cols : 20
	//  Header size = 124
	// imagemagick says this is a regular header
	const char filename12[100] = { "rgb32x32.bmp" };


	test_bitmap(filename0);
//	test_bitmap(filename11);
//	test_bitmap(filename12);

	return EXIT_SUCCESS;
}

#endif
