
// created yyyy-mm-dd 

#ifndef Font_Roboto11_WhiteOnBlue_Character_068_H
#define Font_Roboto11_WhiteOnBlue_Character_068_H

#include "assets.h" 

extern const short int Font_Roboto11_WhiteOnBlue_Character_068_x;
extern const short int Font_Roboto11_WhiteOnBlue_Character_068_y;
extern const unsigned short Font_Roboto11_WhiteOnBlue_Character_068[];
extern const image_t imageFont_Roboto11_WhiteOnBlue_Character_068;


#endif


