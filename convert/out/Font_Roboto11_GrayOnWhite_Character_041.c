
// created yyyy-mm-dd 

#include "assets.h" 

const short int Font_Roboto11_GrayOnWhite_Character_041_x = 6;
const short int Font_Roboto11_GrayOnWhite_Character_041_y = 16;

const unsigned short Font_Roboto11_GrayOnWhite_Character_041[] = {
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x0abc, 0x1f7e, 
0xffff, 0xffff, 0xffff, 0xffff, 0x11dd, 0x2a32, 0x1f7e, 0xffff, 
0xffff, 0xffff, 0xffff, 0x0abc, 0x3132, 0xffbf, 0xffff, 0xffff, 
0xffff, 0x15ff, 0x2a52, 0x1c55, 0xffff, 0xffff, 0xffff, 0xfcff, 
0x2a9b, 0x3533, 0xffdf, 0xffff, 0xffff, 0xffff, 0x0abc, 0x3132, 
0xffbf, 0xffff, 0xffff, 0xffff, 0x0ddd, 0x2d32, 0x1f9f, 0xffff, 
0xffff, 0xffff, 0x0ddd, 0x2d32, 0x1f9f, 0xffff, 0xffff, 0xffff, 
0x0ddd, 0x2d32, 0x1f9f, 0xffff, 0xffff, 0xffff, 0x0abc, 0x3132, 
0xffbf, 0xffff, 0xffff, 0xfcff, 0x2a9b, 0x3533, 0xffdf, 0xffff, 
0xffff, 0x15ff, 0x2a52, 0x1c55, 0xffff, 0xffff, 0xffff, 0x0abc, 
0x3132, 0xffbf, 0xffff, 0xffff, 0x11fe, 0x2a32, 0x1f7e, 0xffff, 
0xffff, 0xffff, 0x2a9b, 0x1f7e, 0xffff, 0xffff, 0xffff, 0xffff, 
};


const image_t imageFont_Roboto11_GrayOnWhite_Character_041 = {
	Font_Roboto11_GrayOnWhite_Character_041_x,
	Font_Roboto11_GrayOnWhite_Character_041_y,
	Font_Roboto11_GrayOnWhite_Character_041
};


