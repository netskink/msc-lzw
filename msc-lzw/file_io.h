#pragma once
int read_file_into_buffer(char *pchFileName, unsigned char **pBuffer);
int write_buffer_to_file(char *pchFileName, unsigned char *pBuffer, unsigned int len);

// The output file name is based upon the tag.
int write_compressed_c_array(list_t *pCompressedListSymbol, char *pchTag);

// The output file name is based upon the tag.
int write_uncompressed_c_array(char *pchOutDirName, unsigned char *pBufferIn, unsigned int len, char *pchTag, int nRows, int nCols);

