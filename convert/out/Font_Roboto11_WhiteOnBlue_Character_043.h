
// created yyyy-mm-dd 

#ifndef Font_Roboto11_WhiteOnBlue_Character_043_H
#define Font_Roboto11_WhiteOnBlue_Character_043_H

#include "assets.h" 

extern const short int Font_Roboto11_WhiteOnBlue_Character_043_x;
extern const short int Font_Roboto11_WhiteOnBlue_Character_043_y;
extern const unsigned short Font_Roboto11_WhiteOnBlue_Character_043[];
extern const image_t imageFont_Roboto11_WhiteOnBlue_Character_043;


#endif


