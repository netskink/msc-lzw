
// created yyyy-mm-dd 

#ifndef Font_Roboto_34_black_on_white_46b_H
#define Font_Roboto_34_black_on_white_46b_H

#include "assets.h" 

extern const short int Font_Roboto_34_black_on_white_46b_x;
extern const short int Font_Roboto_34_black_on_white_46b_y;
extern const unsigned short Font_Roboto_34_black_on_white_46b[];
extern const image_t imageFont_Roboto_34_black_on_white_46b;


#endif


