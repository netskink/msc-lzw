
// created yyyy-mm-dd 

#ifndef Font_Roboto_40_white_on_gray_96_H
#define Font_Roboto_40_white_on_gray_96_H

#include "assets.h" 

extern const short int Font_Roboto_40_white_on_gray_96_x;
extern const short int Font_Roboto_40_white_on_gray_96_y;
extern const unsigned short Font_Roboto_40_white_on_gray_96[];
extern const image_t imageFont_Roboto_40_white_on_gray_96;


#endif


