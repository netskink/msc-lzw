#pragma once
// This list implementation does not do memory copies. It simply does pointer assignments to existing
// storage.  Actually, its been modified so that the append2() method will malloc space and copy
// for int lists.  The string version has not been implemented yet.




/*
record Node {
data; // The data being stored in the node.  Currently it can be either a string or integer.
Node next // A reference to the next node, null for last node
}
record List {
Node firstNode // points to first node of list; null for empty list
}
*/


enum list_add_method {
	eNO_MALLOC,
	eMALLOC_INT, 
	eMALLOC_STRING
};

struct node {
	struct node *pNext;
	void *pData;
};
typedef struct node node_t;

struct list {
	enum list_add_method eMode;
	struct node *pFirst;
	int(*pfCompare)(node_t *, node_t *);  // the function used to compare two nodes.
	int count;  // number of entries in the list
};
typedef struct list list_t;

// This is the original.  It uses pointers to existing data
int append(list_t *pList, void *pData);
// This is convienence version.  It has a method selector which
// will malloc the data and add a copy of the data to the list.
int append2(list_t *pList, void *pData, enum list_add_method eMethod);

//list_t *init_list(void);
list_t *init_list(int(*pFunc)(node_t *, node_t *));

bool is_list_empty(list_t *pList);
bool is_list_singleton(list_t *pList);

list_t * merge_sort(list_t *pList);
list_t * merge(list_t *pListLeft, list_t *pListRight);

int compare_ints(node_t *pNode1, node_t *pNode2);
int compare_strings(node_t *pNode1, node_t *pNode2);

// These two should be one routine with an enum, but i'm in a hurry
void dump_list_of_ints(list_t *pListOfNumbers);
void dump_list_of_shorts(list_t *pListOfNumbers);

void *pop(list_t *pList, int index);
int free_list(list_t *pList);


int convert_list_of_ints_to_array(list_t *pCompressedListSymbol, unsigned short int **pArray, int *pArrayLen);
int convert_array_of_shorts_to_list_of_ints(unsigned short int *pArray, int iArrayLen, list_t **pListInOut);


list_t * reverse(list_t *pList);
void reverse_node(node_t *pFirst, node_t *pNode);

