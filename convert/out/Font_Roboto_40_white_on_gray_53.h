
// created yyyy-mm-dd 

#ifndef Font_Roboto_40_white_on_gray_53_H
#define Font_Roboto_40_white_on_gray_53_H

#include "assets.h" 

extern const short int Font_Roboto_40_white_on_gray_53_x;
extern const short int Font_Roboto_40_white_on_gray_53_y;
extern const unsigned short Font_Roboto_40_white_on_gray_53[];
extern const image_t imageFont_Roboto_40_white_on_gray_53;


#endif


