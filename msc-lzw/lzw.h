#pragma once


const int max_dict_size = 64 * 1024;

unsigned char * decompress(list_t *pListSymbols, unsigned int *pLen);
list_t *compress(unsigned char *pCompressed, unsigned int len);
struct hash_table_int_key_str_value *build_initial_dictionary_intKey_strValue(void);
struct hash_table_str_key_int_value *build_initial_dictionary_strKey_intValue(void);

