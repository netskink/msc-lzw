
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "main.h"
#include "list.h"


#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>	




int append2(list_t *pList, void *pData, enum list_add_method eMethod) {

	int *piValueOrig;
	int *piValueCopy;

	if (eMALLOC_INT == eMethod) {
		pList->eMode = eMALLOC_INT;

		piValueOrig = (int *) pData;
		piValueCopy = (int *) malloc(sizeof(int));
		*piValueCopy = *piValueOrig;
		return append(pList, piValueCopy);

	} else if (eMALLOC_STRING == eMethod) {
		pList->eMode = eMALLOC_STRING;

		return RC_ERROR;
	}

	// no malloc. Just use the existing routine.
	return append(pList, pData);

}


/*
This routine works with pointers.  It does not allocate the data.  It simply adds pointers
to the data appended to the list.

Traversal of a singly linked list is simple,
beginning at the first node and following each next link until we come to the end:

node := list.firstNode
while node not null
(do something with node.data)
node := node.next
*/

int append(list_t *pList, void *pData) {

	node_t *pTip;
	node_t *pNewNode;


	// Creat the new node
	pNewNode = (node_t *) malloc(sizeof(node_t));
	if (NULL == pNewNode) {
		return(-1);
	}

	// Add the data;
	pNewNode->pData = pData;
	pNewNode->pNext = NULL;


	if (NULL == pList->pFirst) {
		pList->pFirst = pNewNode;
	} else {
		pTip = pList->pFirst;
		// Find the last node
		while (NULL != pTip->pNext) {
			pTip = pTip->pNext;
		}
		pTip->pNext = pNewNode;
	}
	pList->count++;

	return(0);
}


list_t *init_list(int(*pFunc)(node_t *, node_t *)) {

	list_t *pList;
	pList = (list_t *) malloc(sizeof(list_t));
	if (NULL == pList) {
		return(NULL);
	}

	pList->pFirst = NULL;
	pList->pfCompare = pFunc;
	pList->count = 0;
	// Assume mode is no alloc.  If we use append2() we overwrite the mode.
	pList->eMode = eNO_MALLOC;


	return (pList);
}

int compare_strings(node_t *pNode1, node_t *pNode2) {
	char *pchBuf1 = (char *)pNode1->pData;
	char *pchBuf2 = (char *)pNode2->pData;

	return strcmp(pchBuf1, pchBuf2);
}
int compare_ints(node_t *pNode1, node_t *pNode2) {

	int val1 = *(int *)pNode1->pData;
	int val2 = *(int *)pNode2->pData;

	if (val1 < val2) {
		return -1;
	}
	else if (val1 == val2) {
		return 0;
	}
	else {
		return 1;
	}
}

bool is_list_empty(list_t *pList) {

	if (NULL == pList->pFirst) {
		return true;
	}
	return false;
}

bool is_list_singleton(list_t *pList) {

	if (NULL == pList->pFirst->pNext) {
		return true;
	}
	return false;
}


/* Rather than use stdlib qsort() (quicksort), implement merge sort based upon
wikipedia pseudo code entry.
https://en.wikipedia.org/wiki/Merge_sort

General idea
Recursively divide the list into smaller sublists
until the sublists are trivially sorted and then
merge the sublists.
*/
list_t *merge_sort(list_t *pList) {

	list_t *pListLeft;
	list_t *pListRight;
	int i = 0;
	node_t *pNode;

	// Base case.  A zero or one element list is sorted by definition
	if (is_list_empty(pList)) {
		return pList;
	}
	if (is_list_singleton(pList)) {
		return pList;
	}

	// The wikipedia entry assumes the number of items in the list
	// is maintained and uses a for loop.  It still requires
	// the list to be iterated.  I'm simply using a while not 
	// empty as I build the sublists. 

	//
	// Create the two sublists
	//
	//	int (*pfCompare)(node_t,node_t);
	pListLeft = init_list(pList->pfCompare);
	if (NULL == pListLeft) {
		printf("Failed to init left sublist.\n");
	}
	pListRight = init_list(pList->pfCompare);
	if (NULL == pListRight) {
		printf("Failed to init right sublist.\n");
	}


	// alternatively add each item of the list to the sublists.
	pNode = pList->pFirst;
	while (NULL != pNode) {
		if (i % 2) {
			if (append(pListLeft, pNode->pData)) {
				printf("Failed to append number to left list.\n");
			}
		}
		else {
			if (append(pListRight, pNode->pData)) {
				printf("Failed to append number to right list.\n");
			}
		}
		pNode = pNode->pNext;
		i++;
	}

	// Recursively sort both sublists
	pListLeft = merge_sort(pListLeft);
	pListRight = merge_sort(pListRight);

	return (merge(pListLeft, pListRight));

}

list_t * merge(list_t *pListLeft, list_t *pListRight) {

	list_t *pListMerged;
	node_t *pHeadLeft;
	node_t *pHeadRight;


	//
	// Create the merged list
	//
	pListMerged = init_list(pListLeft->pfCompare);
	if (NULL == pListMerged) {
		printf("Failed to init Merged list.\n");
	}


	// iterate the two lists appending the values to the
	// merged list
	pHeadLeft = pListLeft->pFirst;
	pHeadRight = pListRight->pFirst;
	while (NULL != pHeadLeft && NULL != pHeadRight) {
		//if ( *(int *) pHeadLeft->pData <=  *(int *) pHeadRight->pData) {
		if (pListLeft->pfCompare(pHeadLeft, pHeadRight) < 0) {
			if (append(pListMerged, pHeadLeft->pData)) {
				printf("Failed to append left side element to merged.\n");
			}
			pHeadLeft = pHeadLeft->pNext;
		}
		else {
			if (append(pListMerged, pHeadRight->pData)) {
				printf("Failed to append right side element to merged.\n");
			}
			pHeadRight = pHeadRight->pNext;
		}
	}

	// Either left or right may have elements left, consume them
	// (only one of the following loops will actually be entered)
	while (NULL != pHeadLeft) {
		if (append(pListMerged, pHeadLeft->pData)) {
			printf("Failed to append left side element to merged.\n");
		}
		pHeadLeft = pHeadLeft->pNext;
	}
	while (NULL != pHeadRight) {
		if (append(pListMerged, pHeadRight->pData)) {
			printf("Failed to append right side element to merged.\n");
		}
		pHeadRight = pHeadRight->pNext;
	}

	return (pListMerged);
}


void dump_list_of_ints(list_t *pListOfNumbers) {

	node_t *pValue = NULL;

	printf("======= dump list of ints\n");

	if (NULL == pListOfNumbers) {
		printf("no list to dump\n");
		return;
	}

	// dump the list starting with the head
	pValue = pListOfNumbers->pFirst;
	while (NULL != pValue) {
		printf("List item as number is %d.\n", *((int *)pValue->pData));
		pValue = pValue->pNext;
	}

}


void dump_list_of_shorts(list_t *pListOfNumbers) {

	node_t *pValue = NULL;

	printf("======= dump list of shorts\n");

	if (NULL == pListOfNumbers) {
		printf("no list to dump\n");
		return;
	}

	// dump the list starting with the head
	pValue = pListOfNumbers->pFirst;
	while (NULL != pValue) {
		printf("List item as number is %d.\n", *( (short int *) pValue->pData ));
		pValue = pValue->pNext;
	}

}


// emulation of python pop method on lists
// It searches for element specified by index
// returns pointer to that reference and then
// removes that item from the list.
// The node is freed.  Its up to the caller to free the data
void *pop(list_t *pList, int index) {

	int iCount = 0;

	node_t *pNode = NULL;
	node_t *pPrev = NULL;
	void *pValue = NULL;

	if (NULL == pList) {
		printf("no list to dump\n");
		return NULL;
	}

	// dump the list starting with the head
	pNode = pList->pFirst;
	while (NULL != pNode && iCount < index) {
		//printf("List item as number is %d.\n", *((int *)pValue->pData));
		pPrev = pNode;
		pNode = pNode->pNext;
		iCount++;
	}

	// assign pValue pointer to the data portion of this node
	pValue = pNode->pData;


	// Remove this Node and reconnect the list.
	if (pList->pFirst == pNode) {
		// this is the first node
		pList->pFirst = pNode->pNext;
	} else {
		// its not the first so connect prev to next.
		pPrev->pNext = pNode->pNext;

	}
	// free the node, but not the data.  The caller will need to free the data
	free(pNode);


	return pValue;

}


int free_list(list_t *pList ) {


	node_t *pNode = NULL;
	node_t *pPrev = NULL;
	void *pValue = NULL;

	if (NULL == pList) {
		printf("no list to dump\n");
		return NULL;
	}

	// dump the list starting with the head
	pNode = pList->pFirst;
	while (NULL != pNode) {
		//printf("List item as number is %d.\n", *((int *)pValue->pData));
		pPrev = pNode;


		// iterate the  list
		pNode = pNode->pNext;

		// Free the prev node
		if (NULL == pPrev->pData) {
			printf("odd. we have a null for the data.\n");
		} else {
			// If the list item was allocated, free it.
			if (eNO_MALLOC != pList->eMode) {
				free(pPrev->pData);
			}
		}
		free(pPrev);
	}

	free(pList);

	return RC_OK;

}


// caller will need to free the pArray
int convert_list_of_ints_to_array(list_t *pCompressedListSymbol, unsigned short int **pArrayInOut, int *pArrayLen) {

	node_t *pValue = NULL;
	unsigned short int *pArray;
	int i;

	printf("======== convert list of short ints to array\n");

	if (NULL == pCompressedListSymbol) {
		printf("no list to convert\n");
		return RC_ERROR;
	}

	// allocate the array based upon the list size
	*pArrayLen = pCompressedListSymbol->count;
	pArray = ( unsigned short int * )malloc(sizeof(unsigned short)*pCompressedListSymbol->count);
	if (NULL == pArray) {
		printf("error in allocating array.\n");
		return RC_ERROR;
	}

	// dump the list starting with the head
	pValue = pCompressedListSymbol->pFirst;
	i = 0;
	while (NULL != pValue) {
		printf("List item as number is %d.\n", *( (int *) pValue->pData ));
		pArray[i] = *( ( unsigned short int * ) pValue->pData );
		pValue = pValue->pNext;
		i++;
	}

	*pArrayInOut = pArray;

	return RC_OK;
}


int convert_array_of_shorts_to_list_of_ints(unsigned short int *pArray, int iArrayLen, list_t **pListInOut) {


	int i;
	int iRC;
	list_t *pListOfNumbers;
	unsigned short int *pVal;
	pListOfNumbers = *pListInOut;


	printf("convert array of shorts to list of ints\n");

	if (NULL == pArray) {
		printf("The array is null.\n");
		return RC_ERROR;
	}

	if (NULL != pListOfNumbers) {
		printf("pListOfNumbers should be null, since it will be allocated in this routine.\n");
		return RC_ERROR;
	}



	//
	// Demo the list with a list of integers using append method
	//


	// Create a linked list of integers.
	pListOfNumbers = init_list(&compare_ints);
	if (NULL == pListOfNumbers) {
		printf("Failed ot init list.\n");
		return RC_ERROR;
	}


	for (i = 0; i < iArrayLen; i++) {

		pVal = &pArray[i];

		printf("array[%d] = %d or %d\n",i, pArray[i], *pVal);

		// int append(list_t *pList, void *pData) 		
		iRC = append(pListOfNumbers, pVal   ); 
//		iRC = append(pListOfNumbers, &pArray[i]);
		if (RC_ERROR == iRC) {
			printf("Failed to append number to list.\n");
			return RC_ERROR;
		}
	}


	dump_list_of_shorts(pListOfNumbers);


	*pListInOut = pListOfNumbers;

	return RC_OK;
}

list_t * reverse(list_t *pList) {

	// early error check
	if (NULL == pList) {
		return NULL;
	}

	reverse_node(pList->pFirst, pList->pFirst);

	return pList;
}

void reverse_node(node_t *pFirst, node_t *pNode) {

	node_t *pPrev;

	// Do all but the last node
	if (NULL == pNode->pNext) {
		pFirst = pNode;
		return;
	}

	// dumps the list in original order, except for the last node
	//printf("val = %d\n", *(int *)pNode->pData);



	reverse_node(pFirst, pNode->pNext);

	
	// dumps the list in reverse order, except for the last node
	//printf("val = %d\n", *(int *) pNode->pData);

	pPrev = pNode->pNext;
	pPrev->pNext = pNode;
	pNode->pNext = NULL;

}



