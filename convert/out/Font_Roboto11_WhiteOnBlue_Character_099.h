
// created yyyy-mm-dd 

#ifndef Font_Roboto11_WhiteOnBlue_Character_099_H
#define Font_Roboto11_WhiteOnBlue_Character_099_H

#include "assets.h" 

extern const short int Font_Roboto11_WhiteOnBlue_Character_099_x;
extern const short int Font_Roboto11_WhiteOnBlue_Character_099_y;
extern const unsigned short Font_Roboto11_WhiteOnBlue_Character_099[];
extern const image_t imageFont_Roboto11_WhiteOnBlue_Character_099;


#endif


