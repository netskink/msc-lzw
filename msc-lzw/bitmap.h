#pragma once

#pragma pack(1)


typedef struct tagMYBITMAPFILEHEADER {
	unsigned short int usiType; // 2 bytes. offset 0
	unsigned int uiSize; //  4 bytes . offset 2
	unsigned short int usiReserved1; // 2 bytes. offset 6
	unsigned short int usiReserved2; // 2 bytes. offset 8
	unsigned int uiOffBits; // 4 bytes. offset 0x0A
} MYBITMAPFILEHEADER, *PMYBITMAPFILEHEADER;


// see https://msdn.microsoft.com/en-us/library/aa383751(VS.85).aspx
// windows type size of 
// WORD    unsigned 16-bit int    2 bytes
// DWORD   unsigned 32-bit int    4 bytes
// LONG    signed 32-bit int      4 bytes

// see https://en.wikipedia.org/wiki/BMP_file_format#DIB_header_.28bitmap_information_header.29
// see https://msdn.microsoft.com/en-us/library/dd183376(v=vs.85).aspx
// bluebattery.bmp uses bitmapinfoheader v5
// rgb32x32.bmp uses bitmapinfoheader v5
typedef struct tagMYBITMAPINFOHEADER {
	unsigned int uiSize;  // 4 bytes. offset 0E - size of this header. 
							// =12 windows 2.0 header
							// =64 os/2 bitmap header2
							// =16 os22xbitmap header
							// =40 Win NT, 3.1 or later bitmap header  BlueBattery.bmp uses this
							// =52 bitmap V2, undocumented
							// =56 bitmap V3, adobe undocumented
							// =108 bitmap V4 Win NT 4.0, 95 or later
							// =124 bitmap V5 winNT 5.0, 98 or later  rgb132x132.bmp uses this
	int iWidth; // 4 bytes. offset 0x12
	int iHeight; // 4 bytes. offset 0x16
	unsigned short int usiPlanes;  // 2 bytes. offset 0x1A, number of planes must be 1
	unsigned short int usiBitCount; // 2 bytes.  offset 0x1C, number of bits per pixel  typical values are 1, 4, 8, 16, 24, 32
	unsigned int uiCompression;  // 4 bytes. offset 0x1E, compression method used. see next table.
	unsigned int uiSizeImage; // 4 bytes. offset 0x22, image size a dummy value of 0 can be given for BI_RGB bitmaps
	int iXPelsPerMeter;  // 4 bytes, offset 0x26, horiz resolution in terms of pixels/meter (signed int)
	int iYPelsPerMeter;  // 4 bytes, offset 0x2A, vert resolution in terms of pixels/meter (signed int)
	unsigned int uiClrUsed;  // 4 bytes, offset 0x2E, number of colors in palatte. 0 or 2^n
	unsigned int uiClrImportant;  // 4 bytes, offset 0x32, number of important colors 0 means every color is important. generally ignored.
} MYBITMAPINFOHEADER, *PMYBITMAPINFOHEADER;

// see https://msdn.microsoft.com/en-us/library/aa383751(VS.85).aspx
// windows type size of 
// WORD    unsigned 16-bit int    2 bytes
// DWORD   unsigned 32-bit int    4 bytes
// LONG    signed 32-bit int      4 bytes

// see https://msdn.microsoft.com/en-us/library/dd183381(v=vs.85).aspx
//

// wingdi.h 
typedef long FXPT2DOT30, *LPFXPT2DOT30;

typedef struct tagMYCIEXYZ {
	FXPT2DOT30 ciexyzX;
	FXPT2DOT30 ciexyzY;
	FXPT2DOT30 ciexyzZ;
} MYCIEXYZ;


typedef struct tagMYCIEXYZTRIPLE {
	MYCIEXYZ ciexyzRed;
	MYCIEXYZ ciexyzGreen;
	MYCIEXYZ ciexyzBlue;
} MYCIEXYZTRIPLE;


typedef struct tagMYBITMAPV5INFOHEADER {
	unsigned int uiSize;  // 4 bytes. offset 0E - size of this header. 
						  // =12 windows 2.0 header
						  // =64 os/2 bitmap header2
						  // =16 os22xbitmap header
						  // =40 Win NT, 3.1 or later bitmap header  BlueBattery.bmp uses this
						  // =52 bitmap V2, undocumented
						  // =56 bitmap V3, adobe undocumented
						  // =108 bitmap V4 Win NT 4.0, 95 or later
						  // =124 bitmap V5 winNT 5.0, 98 or later  rgb132x132.bmp uses this
	int iWidth; // 4 bytes. offset 0x12
	int iHeight; // 4 bytes. offset 0x16
	unsigned short int usiPlanes;  // 2 bytes. offset 0x1A, number of planes must be 1
	unsigned short int usiBitCount; // 2 bytes.  offset 0x1C, number of bits per pixel  typical values are 1, 4, 8, 16, 24, 32
	unsigned int uiCompression;  // 4 bytes. offset 0x1E, compression method used. see next table.
	unsigned int uiSizeImage; // 4 bytes. offset 0x22, image size a dummy value of 0 can be given for BI_RGB bitmaps
	int iXPelsPerMeter;  // 4 bytes, offset 0x26, horiz resolution in terms of pixels/meter (signed int)
	int iYPelsPerMeter;  // 4 bytes, offset 0x2A, vert resolution in terms of pixels/meter (signed int)
	unsigned int uiClrUsed;  // 4 bytes, offset 0x2E, number of colors in palatte. 0 or 2^n
	unsigned int uiClrImportant;  // 4 bytes, offset 0x32, number of important colors 0 means every color is important. generally ignored.
	// Additional fields for V2-V5
	unsigned int uiRedMask;
	unsigned int  uiGreenMask;
	unsigned int  uiBlueMask;
	unsigned int  uiAlphaMask;
	unsigned int  ui5CSType;
	MYCIEXYZTRIPLE  stEndpoints;  // struct having to do with xyz coordinates for specified logical color space
	unsigned int  uiGammaRed;
	unsigned int  uiGammaGreen;
	unsigned int  uiGammaBlue;
	unsigned int  uiIntent;
	unsigned int  uiProfileData;
	unsigned int  uiProfileSize;
	unsigned int  uiReserved;
} MYBITMAPV5INFOHEADER, *PMYBITMAPV5INFOHEADER;

typedef struct tagMYBITMAPV1INFOHEADER {
	unsigned int uiSize;  // 4 bytes. offset 0E - size of this header. 
						  // =12 windows 2.0 header
						  // =64 os/2 bitmap header2
						  // =16 os22xbitmap header
						  // =40 Win NT, 3.1 or later bitmap header  BlueBattery.bmp uses this
						  // =52 bitmap V2, undocumented
						  // =56 bitmap V3, adobe undocumented
						  // =108 bitmap V4 Win NT 4.0, 95 or later
						  // =124 bitmap V5 winNT 5.0, 98 or later  rgb132x132.bmp uses this
	int iWidth; // 4 bytes. offset 0x12
	int iHeight; // 4 bytes. offset 0x16
	unsigned short int usiPlanes;  // 2 bytes. offset 0x1A, number of planes must be 1
	unsigned short int usiBitCount; // 2 bytes.  offset 0x1C, number of bits per pixel  typical values are 1, 4, 8, 16, 24, 32
	unsigned int uiCompression;  // 4 bytes. offset 0x1E, compression method used. see next table.
	unsigned int uiSizeImage; // 4 bytes. offset 0x22, image size a dummy value of 0 can be given for BI_RGB bitmaps
	int iXPelsPerMeter;  // 4 bytes, offset 0x26, horiz resolution in terms of pixels/meter (signed int)
	int iYPelsPerMeter;  // 4 bytes, offset 0x2A, vert resolution in terms of pixels/meter (signed int)
	unsigned int uiClrUsed;  // 4 bytes, offset 0x2E, number of colors in palatte. 0 or 2^n
	unsigned int uiClrImportant;  // 4 bytes, offset 0x32, number of important colors 0 means every color is important. generally ignored.
								  // Additional fields for V2-V5
} MYBITMAPV1INFOHEADER, *PMYBITMAPV1INFOHEADER;

typedef struct tagMYBITMAPV4INFOHEADER {
	unsigned int uiSize;  // 4 bytes. offset 0E - size of this header. 
						  // =12 windows 2.0 header
						  // =64 os/2 bitmap header2
						  // =16 os22xbitmap header
						  // =40 Win NT, 3.1 or later bitmap header  BlueBattery.bmp uses this
						  // =52 bitmap V2, undocumented
						  // =56 bitmap V3, adobe undocumented
						  // =108 bitmap V4 Win NT 4.0, 95 or later
						  // =124 bitmap V5 winNT 5.0, 98 or later  rgb132x132.bmp uses this
	int iWidth; // 4 bytes. offset 0x12
	int iHeight; // 4 bytes. offset 0x16
	unsigned short int usiPlanes;  // 2 bytes. offset 0x1A, number of planes must be 1
	unsigned short int usiBitCount; // 2 bytes.  offset 0x1C, number of bits per pixel  typical values are 1, 4, 8, 16, 24, 32
	unsigned int uiCompression;  // 4 bytes. offset 0x1E, compression method used. see next table.
	unsigned int uiSizeImage; // 4 bytes. offset 0x22, image size a dummy value of 0 can be given for BI_RGB bitmaps
	int iXPelsPerMeter;  // 4 bytes, offset 0x26, horiz resolution in terms of pixels/meter (signed int)
	int iYPelsPerMeter;  // 4 bytes, offset 0x2A, vert resolution in terms of pixels/meter (signed int)
	unsigned int uiClrUsed;  // 4 bytes, offset 0x2E, number of colors in palatte. 0 or 2^n
	unsigned int uiClrImportant;  // 4 bytes, offset 0x32, number of important colors 0 means every color is important. generally ignored.
								  // Additional fields for V2-V5
	unsigned int uiRedMask;
	unsigned int  uiGreenMask;
	unsigned int  uiBlueMask;
	unsigned int  uiAlphaMask;
	unsigned int  ui5CSType;
	MYCIEXYZTRIPLE  stEndpoints;  // struct having to do with xyz coordinates for specified logical color space
	unsigned int  uiGammaRed;
	unsigned int  uiGammaGreen;
	unsigned int  uiGammaBlue;
} MYBITMAPV4INFOHEADER, *PMYBITMAPV4INFOHEADER;



// compression methods
// 0 none
// 1 RLE8 8-bit/pixel
// 2 RLE4 4-bit/pixel
// 3 bitfields
// 4 jpeg
// 5 png
// 6 alphabits
// 11 none
// 12 RLE-8
// 13 RLE-4

// See the respective wiki pages

typedef enum { MYBI_RGB = 0,			// no compression
               MYBI_RLE8 = 1,			// run length encoded format with 8 bpp
               MYBI_RLE4 = 2,			// run length encoded format with 4 bpp
               MYBI_BITFIELDS = 3,	// bitmap is not compressed. bV5RedMask, bV5GreenMask and bV5BlueMask are usable.  Valid for 16 and 32 bpp images.
               MYBI_JPEG = 4,			// image is compressed using JPEG
               MYBI_PNG = 5			// image is compressed using PNG
} bMYV5Compression_t;


typedef enum {
	RG_GB = 0,			// <RG> <GB>
	GB_RG = 1,			// <GB> <RG>
} rgb16_byte_order_t;



int test_bitmap(const char *pchFilename);

// 16-bit to 16-bit
int ExtractImageArray_rgb565_to_rgb565(unsigned char *pBufferIn, int iBufLenAsBytes,
	                                   unsigned int uiRedMask, unsigned int uiGreenMask, unsigned int uiBlueMask,
	                                   unsigned char *pchBufferOut,
	                                   rgb16_byte_order_t eByteOrder,
	                                   int nRows, int nCols);

// 24-bit to 16-bit
int ExtractImageArray_rgb888_to_rgb565(unsigned char *pBufferIn, int iBufLenAsBytes,
	                                   unsigned char *pchBufferOut,
	                                   rgb16_byte_order_t eByteOrder,
	                                   int nRows, int nCols, int pad_size);


int FillAndAllocate(const char* FileName, unsigned char*& buffer_out, unsigned int& BufferSize, rgb16_byte_order_t eByteOrder, int &nRows, int &nCols);

// todo: to reduce the number of casts, change both of these to use "unsigned char  *& buffer" for the first parm
int ParseV5Header(char*& buffer, unsigned int& BufferSize, int &nRows, int &nCols, rgb16_byte_order_t eByteOrder, unsigned char *&buffer_out, unsigned int & buffer_out_size);
int ParseV4Header(char*& buffer, unsigned int& BufferSize, int &nRows, int &nCols, rgb16_byte_order_t eByteOrder, unsigned char *&buffer_out, unsigned int & buffer_out_size);
int ParseV1Header(char*& buffer, unsigned int& BufferSize, int &nRows, int &nCols, rgb16_byte_order_t eByteOrder, unsigned char *&buffer_out, unsigned int & buffer_out_size);

