
// created yyyy-mm-dd 

#ifndef Font_Roboto11_GrayOnWhite_Character_085_H
#define Font_Roboto11_GrayOnWhite_Character_085_H

#include "assets.h" 

extern const short int Font_Roboto11_GrayOnWhite_Character_085_x;
extern const short int Font_Roboto11_GrayOnWhite_Character_085_y;
extern const unsigned short Font_Roboto11_GrayOnWhite_Character_085[];
extern const image_t imageFont_Roboto11_GrayOnWhite_Character_085;


#endif


