
// created yyyy-mm-dd 

#include "assets.h" 

const short int Font_Roboto_34_black_on_white_46b_x = 9;
const short int Font_Roboto_34_black_on_white_46b_y = 3;

const unsigned short Font_Roboto_34_black_on_white_46b[] = {
0x00f8, 0xffff, 0x00f8, 0xffff, 0x00f8, 0xffff, 0x00f8, 0xffff, 
0x00f8, 0xe007, 0xffff, 0xe007, 0xffff, 0xe007, 0xffff, 0xe007, 
0xffff, 0xe007, 0x00f8, 0xe007, 0x1f00, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0x0000, };


const image_t imageFont_Roboto_34_black_on_white_46b = {
	Font_Roboto_34_black_on_white_46b_x,
	Font_Roboto_34_black_on_white_46b_y,
	Font_Roboto_34_black_on_white_46b
};


