
// created yyyy-mm-dd 

#ifndef Font_Roboto11_GrayOnWhite_Character_048_H
#define Font_Roboto11_GrayOnWhite_Character_048_H

#include "assets.h" 

extern const short int Font_Roboto11_GrayOnWhite_Character_048_x;
extern const short int Font_Roboto11_GrayOnWhite_Character_048_y;
extern const unsigned short Font_Roboto11_GrayOnWhite_Character_048[];
extern const image_t imageFont_Roboto11_GrayOnWhite_Character_048;


#endif


