
// created yyyy-mm-dd 

#include "assets.h" 

const short int Font_Roboto11_GrayOnWhite_Character_052_x = 10;
const short int Font_Roboto11_GrayOnWhite_Character_052_y = 22;

const unsigned short Font_Roboto11_GrayOnWhite_Character_052[] = {
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x11fe, 0x2a32, 
0x3533, 0xffdf, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xf8ff, 
0x2a7a, 0x2a32, 0x3533, 0xffdf, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0x0abc, 0x2a32, 0x2a32, 0x3533, 0xffdf, 0xffff, 0xffff, 
0xffff, 0xffff, 0x15ff, 0x2a52, 0x1555, 0x2a7a, 0x3533, 0xffdf, 
0xffff, 0xffff, 0xffff, 0xfcff, 0x2a9b, 0x3533, 0xf8df, 0x2a7a, 
0x3533, 0xffdf, 0xffff, 0xffff, 0xffff, 0x0ddd, 0x2d32, 0x1f9f, 
0xf8ff, 0x2a7a, 0x3533, 0xffdf, 0xffff, 0xffff, 0x15ff, 0x2a52, 
0x1c55, 0xffff, 0xf8ff, 0x2a7a, 0x3533, 0xffdf, 0xffff, 0xffff, 
0x0abc, 0x2a32, 0x2a32, 0x2a32, 0x2a32, 0x2a32, 0x2a32, 0x2d32, 
0x1f9f, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xf8ff, 0x2a7a, 
0x3533, 0xffdf, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xf8ff, 0x2a7a, 0x3533, 0xffdf, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xf8ff, 0x2a7a, 0x3533, 0xffdf, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, };


const image_t imageFont_Roboto11_GrayOnWhite_Character_052 = {
	Font_Roboto11_GrayOnWhite_Character_052_x,
	Font_Roboto11_GrayOnWhite_Character_052_y,
	Font_Roboto11_GrayOnWhite_Character_052
};


