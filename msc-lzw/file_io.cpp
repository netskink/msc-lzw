
#include "stdafx.h"

#include <string.h>
#include <stdio.h>
#include "main.h"
#include "dictionary.h"
#include "list.h"
#include "file_io.h"

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>	



//
// https://docs.microsoft.com/en-us/cpp/c-runtime-library/file-handling
//

// Allocates the buffer, reads the file contents into the buffer
// and adds a null terminating zero to make it a string.
int read_file_into_buffer(char *pchFileName, unsigned char **pchBuffer) {

	unsigned char *buffer;
//	int size;
	size_t size;
//	int count = 0;
	size_t count = 0;
	FILE *source;
//	int n;
	size_t n;

	source = fopen((char *)pchFileName, "rb");
	//fopen_s(&source, pchFileName, "rb");

	if (NULL == source) {
		printf("fail to open file for read\n");
		return -1;
	}

	fseek(source, 0L, SEEK_END);  // seek to end of file
	size = ftell(source);  // get current position of file pointer, this buffer will not have a null term byte

	// allocate buffer equivalent to file size
	buffer = (unsigned char *)malloc(size * sizeof(char));

	fseek(source, 0L, SEEK_SET);  // Seek to start of file to begin read
	while (!feof(source)) {
		n = fread(buffer, 1, size, source);
		count += n;
	}
	printf("%d bytes read.\n", (int) count);

	fclose(source);

	*pchBuffer = buffer;

	return (unsigned int) count;  // I use a signed int so I can return -1 if an error occurs.
}






int write_buffer_to_file(char *pchFileName, unsigned char *pBuffer, unsigned int len) {

//	unsigned int count = 0;
	size_t count = 0;
	FILE *dest;
//	int n;
	size_t n;

	//dest = fopen(pchFileName, "wb");
	fopen_s(&dest, (char *) pchFileName, "wb");

	if (NULL == dest) {
		printf("fail to open file for write\n");
		return RC_ERROR;
	}

	while (count < len) {
		n = fwrite(pBuffer, 1, len, dest);
		count += n;
	}
	printf("%d bytes written.\n", (int) count);

	fclose(dest);

	return RC_OK;
}

int write_compressed_c_array(list_t *pCompressedListSymbol,  char *pchTag) {

	int iRC = RC_ERROR;  // Assume error
	size_t count = 0;  // used to count number of bytes written to file and for length of tag
//	int length; // used for various lengths
	FILE *c_file;		// output file pointer
	FILE *h_file;		// output file pointer
	size_t n;
	const int buff_size = 512;  // max line size
	char pBuffer[buff_size];  // used for file writes.  The most we will write at anyone time is 256 bytes
	char pchFileName_dot_c[buff_size];  // c src file. Filename is at most 256 chars
	char pchFileName_dot_h[buff_size];  // c header file Filename is at most 256 chars
	int num_list_items;
	unsigned int symbol;
	int num_shorts = 1;



	node_t *pValue = NULL;

	if (NULL == pCompressedListSymbol) {
		printf("no list to write to c file\n");
		return iRC;
	}




	// Determine c src file name
	//count = strlen(pchTag);
	strcpy(pchFileName_dot_c, "compressed_");
	strcat(pchFileName_dot_c, pchTag);
	strcat(pchFileName_dot_c, ".c");


	// Determine c header file name
	strcpy(pchFileName_dot_h, "compressed_");
	strcat(pchFileName_dot_h, pchTag);
	strcat(pchFileName_dot_h, ".h");


	//dest = fopen(pchFileName, "wb");
	fopen_s(&c_file, pchFileName_dot_c, "w");
	fopen_s(&h_file, pchFileName_dot_h, "w");

	if (NULL == c_file) {
		printf("fail to open c file for write\n");
		return RC_ERROR;
	}
	if (NULL == h_file) {
		printf("fail to open h file for write\n");
		return RC_ERROR;
	}

	// How many items will be in the array? Its the number of items in the list.
	num_list_items = pCompressedListSymbol->count;


	// Write the boilerplate the .h file
	//
	// #ifdef FOO_H
	// #define FOO_H
	// extern const unsigned int TAG_size;
	// extern unsigned short compressed_TAG[];

	sprintf(pBuffer, "#ifndef compressed_%s_H\n", pchTag);
	count = strlen(pBuffer);  // do not include a mod for num term zero
	n = fwrite(pBuffer, 1, count, h_file);
	sprintf(pBuffer, "#define %s_H\n", pchTag);
	count = strlen(pBuffer);  // do not include a mod for num term zero
	n = fwrite(pBuffer, 1, count, h_file);
	sprintf(pBuffer, "extern const unsigned int compressed_%s_size;\n", pchTag);
	count = strlen(pBuffer);  // do not include a mod for num term zero
	n = fwrite(pBuffer, 1, count, h_file);
	sprintf(pBuffer, "extern const unsigned short compressed_%s[];\n", pchTag);
	count = strlen(pBuffer);  // do not include a mod for num term zero
	n = fwrite(pBuffer, 1, count, h_file);

	// Write the boilerplate to the .c file.
	// 
	// const unsigned int TAG_size = <size>;
	// extern unsigned short compressedTAG[TAG_size] = {

	sprintf(pBuffer, "const unsigned int compressed_%s_size = %d;\n", pchTag, num_list_items);
	count = strlen(pBuffer);  // do not include a mod for num term zero
	n = fwrite(pBuffer, 1, count, c_file);
	sprintf(pBuffer, "const unsigned short compressed_%s[compressed_%s_size] = {\n", pchTag, pchTag);
	count = strlen(pBuffer);  // do not include a mod for num term zero
	n = fwrite(pBuffer, 1, count, c_file);

	// write the compressed symbols as hex shorts
	pValue = pCompressedListSymbol->pFirst;
	while (NULL != pValue) {
		// TODO: double check that these are unsigned ints
		symbol = *( (unsigned int *) pValue->pData );
		printf("List item as number is %d.\n", symbol);

		sprintf(pBuffer, "0x%04x, ", symbol);
		count = strlen(pBuffer);  // do not include a mod for num term zero
		n = fwrite(pBuffer, 1, count, c_file);

		// every 8 shorts, write a newline
		if (0 == num_shorts % 8) {
			strcpy(pBuffer, "\n");
			count = strlen(pBuffer);  // do not include a mod for num term zero
			n = fwrite(pBuffer, 1, count, c_file);

		}

		num_shorts++;


		pValue = pValue->pNext;
	}


	// Write the trailing boilerplate for the header file
	sprintf(pBuffer, "#endif\n\n\n");
	count = strlen(pBuffer) + 0;  // +1 for <null term zero>
	n = fwrite(pBuffer, 1, count, h_file);

	// Write the trailing boilerplate for the source file
	sprintf(pBuffer, "};");
	count = strlen(pBuffer) + 0;  // +1 for <null term zero>
	n = fwrite(pBuffer, 1, count, c_file);


	fclose(c_file);
	fclose(h_file);


	iRC = RC_OK;
	return iRC;
}

// The output filenames are based upon the tag.
int write_uncompressed_c_array(char *pchOutDirName, unsigned char *pBufferIn, unsigned int len, char *pchTag, int nRows, int nCols) {

	int iRC = RC_ERROR;  // Assume error
	size_t count = 0;  // used to count number of bytes written to file and for length of tag
					   //	int length; // used for various lengths
	FILE *c_file;		// output file pointer
	FILE *h_file;		// output file pointer
	size_t n;
	const int buff_size = 512;  // max line size
	char pBuffer[buff_size];  // used for file writes.  The most we will write at anyone time is 256 bytes
	char pchFileName_dot_c[buff_size];  // c src file. Filename is at most 256 chars
	char pchFileName_dot_h[buff_size];  // c header file Filename is at most 256 chars
	int num_shorts = 1;




	if (NULL == pBuffer) {
		printf("no buffer to write to c file\n");
		return iRC;
	}


	// prepend dir name
	strcpy(pchFileName_dot_c, pchOutDirName);
	strcat(pchFileName_dot_c, "\\");
	strcpy(pchFileName_dot_h, pchOutDirName);
	strcat(pchFileName_dot_h, "\\");

	// Determine c src file name
	//count = strlen(pchTag);
	strcat(pchFileName_dot_c, pchTag);
	strcat(pchFileName_dot_c, ".c");


	// Determine c header file name
	strcat(pchFileName_dot_h, pchTag);
	strcat(pchFileName_dot_h, ".h");


	//dest = fopen(pchFileName, "wb");
	fopen_s(&c_file, pchFileName_dot_c, "w");
	fopen_s(&h_file, pchFileName_dot_h, "w");

	if (NULL == c_file) {
		printf("fail to open c file for write\n");
		return RC_ERROR;
	}
	if (NULL == h_file) {
		printf("fail to open h file for write\n");
		return RC_ERROR;
	}


	// Write the boilerplate the .h file
	//
	// // created yyyy-mm-dd
	// 
	// #ifdef TAG_H
	// #define TAG_H
	//
	// #include "assets.h"
	//
	// extern const short int TAG_x;
	// extern const short int TAG_y;
	// extern unsigned short TAG[];
	// extern const image_t  imageTAG;

	// // created yyyy mm dd
	sprintf(pBuffer, "\n// created yyyy-mm-dd \n\n");  // TODO: determine date stamp
	count = strlen(pBuffer);  // do not include a mod for num term zero
	n = fwrite(pBuffer, 1, count, h_file);


	// #ifndef TAG_H
	sprintf(pBuffer, "#ifndef %s_H\n", pchTag);
	count = strlen(pBuffer);  // do not include a mod for num term zero
	n = fwrite(pBuffer, 1, count, h_file);

	// #define TAG_H
	sprintf(pBuffer, "#define %s_H\n\n", pchTag);
	count = strlen(pBuffer);  // do not include a mod for num term zero
	n = fwrite(pBuffer, 1, count, h_file);

	// #include "assets.h"
	sprintf(pBuffer, "#include \"assets.h\" \n\n");
	count = strlen(pBuffer);  // do not include a mod for num term zero
	n = fwrite(pBuffer, 1, count, h_file);



	// extern const short int TAG_x;
	// extern const short int TAG_y;
	sprintf(pBuffer, "extern const short int %s_x;\n", pchTag);
	count = strlen(pBuffer);  // do not include a mod for num term zero
	n = fwrite(pBuffer, 1, count, h_file);
	sprintf(pBuffer, "extern const short int %s_y;\n", pchTag);
	count = strlen(pBuffer);  // do not include a mod for num term zero
	n = fwrite(pBuffer, 1, count, h_file);


	// extern unsigned short TAG[];
	sprintf(pBuffer, "extern const unsigned short %s[];\n", pchTag);
	count = strlen(pBuffer);  // do not include a mod for num term zero
	n = fwrite(pBuffer, 1, count, h_file);

	// extern const image_t  imageTAG;
	sprintf(pBuffer, "extern const image_t image%s;\n\n\n", pchTag);
	count = strlen(pBuffer);  // do not include a mod for num term zero
	n = fwrite(pBuffer, 1, count, h_file);


	// Write the boilerplate to the .c file.
	// 
	//
	// // created yyyy-mm-dd
	// 
	//
	// #include "assets.h"
	//
	// const short int TAG_x = n;
	// const short int TAG_y = n;
	//
	// const unsigned short TAG[] = {



	// // created yyyy mm dd
	sprintf(pBuffer, "\n// created yyyy-mm-dd \n\n");  // TODO: determine date stamp
	count = strlen(pBuffer);  // do not include a mod for num term zero
	n = fwrite(pBuffer, 1, count, c_file);

	// #include "assets.h"
	sprintf(pBuffer, "#include \"assets.h\" \n\n");
	count = strlen(pBuffer);  // do not include a mod for num term zero
	n = fwrite(pBuffer, 1, count, c_file);


	// const short int TAG_x = n;
	// const short int TAG_y = n;
	sprintf(pBuffer, "const short int %s_x = %d;\n", pchTag, nCols);
	count = strlen(pBuffer);  // do not include a mod for num term zero
	n = fwrite(pBuffer, 1, count, c_file);
	sprintf(pBuffer, "const short int %s_y = %d;\n\n", pchTag, nRows);
	count = strlen(pBuffer);  // do not include a mod for num term zero
	n = fwrite(pBuffer, 1, count, c_file);



	// const unsigned short TAG[] = {
	sprintf(pBuffer, "const unsigned short %s[] = {\n", pchTag);
	count = strlen(pBuffer);  // do not include a mod for num term zero
	n = fwrite(pBuffer, 1, count, c_file);




	// write the uncompressed buffers as hex shorts
	unsigned short int *pValue = NULL;

	unsigned int i = 0;
	pValue = (unsigned short int *) pBufferIn;
	// len is in terms of bytes. the pointer for pVAlue is a short. 
	// adjust the len by 2
	len = len / 2;
	while (i < len) {
		// TODO: double check that these are unsigned ints

		sprintf(pBuffer, "0x%04x, ", *pValue);
		count = strlen(pBuffer);  // do not include a mod for num term zero
		n = fwrite(pBuffer, 1, count, c_file);

		// every 8 shorts, write a newline
		if (0 == num_shorts % 8) {
			strcpy(pBuffer, "\n");
			count = strlen(pBuffer);  // do not include a mod for num term zero
			n = fwrite(pBuffer, 1, count, c_file);

		}

		num_shorts++;

		// get next pixel
		i++;
		pValue++;
	}
	// Write the trailing boilerplate for the array in source file
	sprintf(pBuffer, "};\n\n\n");
	count = strlen(pBuffer) + 0;  // +1 for <null term zero>
	n = fwrite(pBuffer, 1, count, c_file);




	// Write the wrapper struct for the .c file

	//  const image_t imageTAG = {
	//	  TAG_x,
	//	  TAG_y,
	//	  TAG
	//  };

	// const image_t imageTAG[] = {
	sprintf(pBuffer, "const image_t image%s = {\n", pchTag);
	count = strlen(pBuffer);  // do not include a mod for num term zero
	n = fwrite(pBuffer, 1, count, c_file);


	//   TAG_x,
	//   TAG_y,
	sprintf(pBuffer, "\t%s_x,\n", pchTag);
	count = strlen(pBuffer);  // do not include a mod for num term zero
	n = fwrite(pBuffer, 1, count, c_file);
	sprintf(pBuffer, "\t%s_y,\n", pchTag);
	count = strlen(pBuffer);  // do not include a mod for num term zero
	n = fwrite(pBuffer, 1, count, c_file);



	// TAG[]
	sprintf(pBuffer, "\t%s\n", pchTag);
	count = strlen(pBuffer);  // do not include a mod for num term zero
	n = fwrite(pBuffer, 1, count, c_file);






	// Write the trailing boilerplate for the struct wrapper in the source file
	sprintf(pBuffer, "};\n\n\n");
	count = strlen(pBuffer) + 0;  // +1 for <null term zero>
	n = fwrite(pBuffer, 1, count, c_file);




	// Write the trailing boilerplate for the header file
	sprintf(pBuffer, "#endif\n\n\n");
	count = strlen(pBuffer) + 0;  // +1 for <null term zero>
	n = fwrite(pBuffer, 1, count, h_file);



	fclose(c_file);
	fclose(h_file);




	return RC_OK;
}
