
// created yyyy-mm-dd 

#ifndef Font_Roboto11_GrayOnWhite_Character_110_H
#define Font_Roboto11_GrayOnWhite_Character_110_H

#include "assets.h" 

extern const short int Font_Roboto11_GrayOnWhite_Character_110_x;
extern const short int Font_Roboto11_GrayOnWhite_Character_110_y;
extern const unsigned short Font_Roboto11_GrayOnWhite_Character_110[];
extern const image_t imageFont_Roboto11_GrayOnWhite_Character_110;


#endif


