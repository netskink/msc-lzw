
// created yyyy-mm-dd 

#ifndef Font_Roboto11_Bold_GrayOnWhite_Character_108_H
#define Font_Roboto11_Bold_GrayOnWhite_Character_108_H

#include "assets.h" 

extern const short int Font_Roboto11_Bold_GrayOnWhite_Character_108_x;
extern const short int Font_Roboto11_Bold_GrayOnWhite_Character_108_y;
extern const unsigned short Font_Roboto11_Bold_GrayOnWhite_Character_108[];
extern const image_t imageFont_Roboto11_Bold_GrayOnWhite_Character_108;


#endif


