
// created yyyy-mm-dd 

#include "assets.h" 

const short int Font_Roboto11_WhiteOnBlue_Character_116_x = 6;
const short int Font_Roboto11_WhiteOnBlue_Character_116_y = 16;

const unsigned short Font_Roboto11_WhiteOnBlue_Character_116[] = {
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1f77, 0xffff, 0x9d95, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1f77, 0xffff, 0x9d95, 0x1d2d, 0x1d2d, 0x7f97, 0xffff, 
0xffff, 0xffff, 0x9dde, 0x1d2d, 0x1d2d, 0x1f77, 0xffff, 0x9d95, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1f77, 0xffff, 0x9d95, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1f77, 0xffff, 0x9d95, 0x1d2d, 0x1d2d, 0x1d2d, 0x1f77, 
0xffff, 0x9d95, 0x1d2d, 0x1d2d, 0x1d2d, 0x1f77, 0xffff, 0x9d95, 
0x1d2d, 0x1d2d, 0x1d2d, 0x9f56, 0xffff, 0x9d95, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1e2d, 0xffbf, 0xffff, 0x1eff, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 0x1d2d, 
};


const image_t imageFont_Roboto11_WhiteOnBlue_Character_116 = {
	Font_Roboto11_WhiteOnBlue_Character_116_x,
	Font_Roboto11_WhiteOnBlue_Character_116_y,
	Font_Roboto11_WhiteOnBlue_Character_116
};


