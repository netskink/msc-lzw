
// created yyyy-mm-dd 

#include "assets.h" 

const short int Font_Roboto11_Bold_GrayOnWhite_Character_126_x = 7;
const short int Font_Roboto11_Bold_GrayOnWhite_Character_126_y = 16;

const unsigned short Font_Roboto11_Bold_GrayOnWhite_Character_126[] = {
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0x11fe, 
0x2a32, 0x2a32, 0x2d32, 0x1f9f, 0xffff, 0xf8ff, 0x2a7a, 0x3132, 
0x11be, 0x2a32, 0x1834, 0xffff, 0xf8ff, 0x2a7a, 0x3132, 0x11be, 
0x2a32, 0x3533, 0xffdf, 0xffff, 0x0ddd, 0x2a32, 0x2a32, 0x2d32, 
0x1f9f, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 0xffff, 
};


const image_t imageFont_Roboto11_Bold_GrayOnWhite_Character_126 = {
	Font_Roboto11_Bold_GrayOnWhite_Character_126_x,
	Font_Roboto11_Bold_GrayOnWhite_Character_126_y,
	Font_Roboto11_Bold_GrayOnWhite_Character_126
};


