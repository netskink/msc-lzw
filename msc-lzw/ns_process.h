#pragma once

int didCompressionDecompressionWork(const char *pchFileName1, char *pchFileName2);
void CreateChildProcess(const char *pchFileName1, char *pchFileName2);
int ReadFromPipe(unsigned char *pchBuffer, unsigned long int bufLen);
void ErrorExit(PTSTR);
int parseBuffer(unsigned char *pchBuffer, unsigned long int bufLen);
